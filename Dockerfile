


#  ____           _ __  __  ___  
# |  _ \ ___   __| |  \/  |/ _ \
# | |_) / _ \ / _` | |\/| | | | |
# |  __/ (_) | (_| | |  | | |_| |
# |_|   \___/ \__,_|_|  |_|\__\_\

# Network Message Queue In C.
# Copyright (C) 2019 Zach Podbielniak

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


FROM zachpodbielniak/podnet:latest

WORKDIR /podmq
COPY . .

RUN make -j
RUN make debug -j
RUN make install
RUN make install_debug
RUN make install_headers
RUN make server
RUN make server_debug

EXPOSE 9002
CMD ["./bin/podmq-server", "-p", "9002", "-b", "0.0.0.0"]