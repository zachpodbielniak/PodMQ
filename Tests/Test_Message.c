/*


 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <glib-object.h>
#include "../Src/Message/Message.h"



gint main(
	gint			argc,
	gchar			**argv
){
	PodMQMessage *msg;
	PodMQMessage *msg_from_json;
	GArray *headers;
	PodMQHeader header;
	gchar *data;
	gchar *pretty;
	gchar *test = "{\"header\":{\"queue_name\":\"test_queue_name\",\"trace_id\":\"wlkja09u41084014982123\",\"timestamp\":1664054324008296,\"request_type\":1,\"message_length\":24,\"test_header_1\":\"test_value_1\",\"test_header_2\":\"test_value_2\"},\"body\":\"this is the message body\"}";

	headers = g_array_new(TRUE, TRUE, sizeof(PodMQHeader));

	header.key = g_strdup("test_header_1");
	header.value = g_strdup("test_value_1");
	g_array_append_val(headers, header);	
	
	header.key = g_strdup("test_header_2");
	header.value = g_strdup("test_value_2");
	g_array_append_val(headers, header);	

	msg = podmq_message_new(
		"test_queue_name",
		"this is the message body",
		"wlkja09u41084014982123",
		headers,
		PODMQ_PUT
	);

	data = (gchar *)podmq_message_get_data_raw(msg);
	g_print("Data: %s\n", data);

	pretty = (gchar *)podmq_message_get_data_pretty(msg);
	g_print("Pretty:\n%s\n", pretty);

	msg_from_json = podmq_message_new_from_json(test);

	data = (gchar *)podmq_message_get_data_raw(msg_from_json);
	g_print("Data: %s\n", data);

	pretty = (gchar *)podmq_message_get_data_pretty(msg_from_json);
	g_print("Pretty:\n%s\n", pretty);

	g_object_unref(msg_from_json);
	g_object_unref(msg);
	return 0;
}