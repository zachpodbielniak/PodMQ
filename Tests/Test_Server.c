/*


 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <glib-object.h>
#include "../Src/Queue/Queue.h"
#include "../Src/Queue/QueueManager.h"
#include "../Src/Server/Server.h"



gint 
main(
	gint			arg,
	gchar			**argv
){
	PodMQMessage *msg;
	PodMQMessage *msg2;
	PodMQQueue *queue;
	PodMQQueue *queue2;
	PodMQQueue *queue3;
	PodMQQueueManager *manager;
	PodMQServer *server;
	gchar *test;
	gchar *test2;
	GArray *tokens;
	gchar *token;
	gint i;

	test = "{\"header\":{\"queue_name\":\"test_queue_name\",\"trace_id\":\"wlkja09u41084014982123\",\"timestamp\":1664054324008296,\"request_type\":1,\"message_length\":24,\"test_header_1\":\"test_value_1\",\"test_header_2\":\"test_value_2\"},\"body\":\"this is the message body\"}";
	test2 = "{\"header\":{\"queue_name\":\"test_queue_name\",\"trace_id\":\"wlkja09u41084014989847\",\"timestamp\":1664054324008296,\"request_type\":1,\"message_length\":24,\"test_header_1\":\"test_value_1\",\"test_header_2\":\"test_value_2\"},\"body\":\"this is the message body 2\"}";

	tokens = g_array_new(TRUE, TRUE, sizeof(const gchar *));
	token = "1234";
	g_array_append_val(tokens, token);

	msg = podmq_message_new_from_json(test);
	msg2 = podmq_message_new_from_json(test2);
	queue = podmq_queue_new("test_queue");
	queue2 = podmq_queue_new("bank_transactions");
	queue3 = podmq_queue_new("store_orders");

	manager = podmq_queue_manager_new("manager");
	podmq_queue_manager_add(manager, queue);
	podmq_queue_manager_add(manager, queue2);
	podmq_queue_manager_add(manager, queue3);
		
	podmq_queue_put(queue, msg);
	podmq_queue_put(queue, msg2);
	
	for (
		i = 0;
		i < 1000;
		i++
	){
		PodMQMessage *temp1;
		PodMQMessage *temp2;

		temp1 = podmq_message_copy(msg);
		temp2 = podmq_message_copy(msg2);

		podmq_queue_put(queue, temp1);
		podmq_queue_put(queue, temp2);
	}

	server = podmq_server_new("0.0.0.0", 9001, tokens);
	podmq_server_set_default_manager(server, manager);
	podmq_server_serve(server);

	return 0;
}