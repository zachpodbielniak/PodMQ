/*


 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <glib-object.h>
#include "../Src/Message/Message.h"
#include "../Src/Client/Client.h"



gint 
main(
	gint			arg,
	gchar			**argv
){
	g_autoptr(PodMQMessage) msg;
	g_autoptr(PodMQMessage) msg_put;
	g_autoptr(PodMQMessage) msg_put2;
	g_autoptr(PodMQClient) client;
	gchar *test;
	gint i;
	GArray *messages;

	msg = NULL;
	msg_put = NULL;
	client = NULL;

	test = "{\"header\":{\"queue_name\":\"test_queue_name\",\"trace_id\":\"wlkja09u41084014982123\",\"timestamp\":1664054324008296,\"request_type\":1,\"message_length\":24,\"test_header_1\":\"test_value_1\",\"test_header_2\":\"test_value_2\"},\"body\":\"this is the message body\"}";

	client = podmq_client_new(
		"localhost",
		9001,
		"default",
		"default",
		NULL,
		FALSE
	);

	msg_put = podmq_message_new_from_json(test);
	msg_put2 = podmq_message_copy(msg_put);
	messages = g_array_new(TRUE, TRUE, sizeof(PodMQMessage *));

	g_array_append_val(messages, msg_put);
	g_array_append_val(messages, msg_put2);

	for (
		i = 0;
		i < 5;
		i++
	){
		podmq_client_put_many(client, messages);
	}

	// GArray *messages;
	// messages = podmq_client_peek(client);

	// for (
	// 	i = 0;
	// 	i < (gint)messages->len;
	// 	i++
	// ){
	// 	PodMQMessage *msg_peek;

	// 	msg_peek = g_array_index(messages, PodMQMessage *, i);
	// 	g_print("%s\n", podmq_message_get_data_pretty(msg_peek));

	// 	g_object_unref(msg_peek);
	// }

	// g_array_unref(messages);


	return 0;
}