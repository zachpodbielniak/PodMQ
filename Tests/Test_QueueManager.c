/*


 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <glib-object.h>
#include "../Src/Queue/Queue.h"
#include "../Src/Queue/QueueManager.h"



gint 
main(
	gint			arg,
	gchar			**argv
){
	PodMQMessage *msg;
	PodMQMessage *get;
	PodMQQueue *queue;
	PodMQQueue *queue_get;
	PodMQQueueManager *manager;
	gchar *test;
	gchar *data;

	test = "{\"header\":{\"queue_name\":\"test_queue_name\",\"trace_id\":\"wlkja09u41084014982123\",\"timestamp\":1664054324008296,\"request_type\":1,\"message_length\":24,\"test_header_1\":\"test_value_1\",\"test_header_2\":\"test_value_2\"},\"body\":\"this is the message body\"}";

	msg = podmq_message_new_from_json(test);
	queue = podmq_queue_new("test_queue");

	manager = podmq_queue_manager_new("manager");
	podmq_queue_manager_add(manager, queue);

	queue_get = podmq_queue_manager_get_queue_by_name(manager, "test_queue");

	podmq_queue_put(queue_get, msg);
	get = podmq_queue_get(queue_get);

	g_print("Got message:\n%s\n", podmq_message_get_data_pretty(get));

	g_object_unref(manager);
	g_object_unref(queue);
	g_object_unref(msg);
	return 0;
}