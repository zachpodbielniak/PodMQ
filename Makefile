# Makefile for AudioRouter

CC = gcc
STD = -std=gnu89
WARNINGS = -Wall -Wextra -Wno-unused-parameter -Wno-int-conversion
DEFINES = -D _DEFAULT_SOURCE
DEFINES = -D _GNU_SOURCE

DEFINES_D = $(DEFINES)
DEFINES_D += -D __DEBUG__

OPTIMIZE = -O3 -funroll-loops -fstrict-aliasing
OPTIMIZE += -fstack-protector-strong
MARCH = -march=native
MTUNE = -mtune=native

C_FLAGS = $(STD)
C_FLAGS += $(WARNINGS)
C_FLAGS += $(DEFINES)
C_FLAGS += $(OPTIMIZE)
C_FLAGS += $(MARCH)
C_FLAGS += $(MTUNE)
C_FLAGS += $(shell pkg-config gio-2.0 --cflags --libs)
C_FLAGS += $(shell pkg-config json-glib-1.0 --cflags --libs)
C_FLAGS += $(shell pkg-config gobject-2.0 --cflags --libs)
C_FLAGS += $(shell pkg-config libsoup-3.0 --cflags --libs)

C_FLAGS_D = $(STD)
C_FLAGS_D += $(WARNINGS)
C_FLAGS_D += $(DEFINES_D)
C_FLAGS_D += $(shell pkg-config gio-2.0 --cflags --libs)
C_FLAGS_D += $(shell pkg-config json-glib-1.0 --cflags --libs)
C_FLAGS_D += $(shell pkg-config gobject-2.0 --cflags --libs)
C_FLAGS_D += $(shell pkg-config libsoup-3.0 --cflags --libs)

C_FLAGS_T = $(C_FLAGS_D)
C_FLAGS_T += -Wno-unused-variable


FILES  = Message.o Queue.o QueueManager.o Server.o Client.o

FILES_D  = Message_d.o Queue_d.o QueueManager_d.o Server_d.o Client_d.o




all: libpodmq.so

debug: libpodmq_d.so 

CServer: podmq.o

CServer_debug: podmq_d.o

CLIent: CLIent.o 
CLIent_debug: CLIent_d.o

dir: 
	mkdir -p bin

clean: dir
	rm -rf bin

install:
	cp bin/libpodmq.so /usr/lib/

install_debug:
	cp bin/libpodmq_d.so /usr/lib/

install_headers:
	mkdir -p /usr/local/include/PodMQ/
	find . -type f -name "*.h" -exec install -D {} /usr/local/include/PodMQ/{} \;

install_python:
	mkdir -p /usr/lib/python3/dist-packages/PodMQ
	cp -r Src/Python/* /usr/lib/python3/dist-packages/PodMQ



standalone: dir
	$(CC) -o bin/podmq Src/Standalone/Standalone.c -lpodmq $(C_FLAGS)

standalone_debug: dir
	$(CC) -o bin/podmq -g Src/Standalone/Standalone.c -lpodmq_d $(C_FLAGS_D)



libpodmq.so: dir $(FILES) 
	$(CC) -shared -fPIC -s -o bin/libpodmq.so bin/*.o $(C_FLAGS)
	rm bin/*.o


libpodmq_d.so: dir $(FILES_D) 
	$(CC) -g -shared -fPIC -o bin/libpodmq_d.so bin/*_d.o $(C_FLAGS_D)
	rm bin/*.o




Message.o: dir
	$(CC) -fPIC -c -o bin/Message.o Src/Message/Message.c $(C_FLAGS)
Message_d.o: dir
	$(CC) -g -fPIC -c -o bin/Message_d.o Src/Message/Message.c $(C_FLAGS_D)


Queue.o: dir
	$(CC) -fPIC -c -o bin/Queue.o Src/Queue/Queue.c $(C_FLAGS)
Queue_d.o: dir
	$(CC) -g -fPIC -c -o bin/Queue_d.o Src/Queue/Queue.c $(C_FLAGS_D)


QueueManager.o: dir
	$(CC) -fPIC -c -o bin/QueueManager.o Src/Queue/QueueManager.c $(C_FLAGS)
QueueManager_d.o: dir
	$(CC) -g -fPIC -c -o bin/QueueManager_d.o Src/Queue/QueueManager.c $(C_FLAGS_D)


Server.o: dir
	$(CC) -fPIC -c -o bin/Server.o Src/Server/Server.c $(C_FLAGS)
Server_d.o: dir
	$(CC) -g -fPIC -c -o bin/Server_d.o Src/Server/Server.c $(C_FLAGS_D)


Client.o: dir
	$(CC) -fPIC -c -o bin/Client.o Src/Client/Client.c $(C_FLAGS)
Client_d.o: dir
	$(CC) -g -fPIC -c -o bin/Client_d.o Src/Client/Client.c $(C_FLAGS_D)





Test_Message: dir
	$(CC) -g -fPIC -o bin/Test_Message Tests/Test_Message.c -lpodmq_d $(C_FLAGS_D)
Test_Queue: dir
	$(CC) -g -fPIC -o bin/Test_Queue Tests/Test_Queue.c -lpodmq_d $(C_FLAGS_D)
Test_QueueManager: dir
	$(CC) -g -fPIC -o bin/Test_QueueManager Tests/Test_QueueManager.c -lpodmq_d $(C_FLAGS_D)
Test_Server: dir
	$(CC) -g -fPIC -o bin/Test_Server Tests/Test_Server.c -lpodmq_d $(C_FLAGS_D)
Test_Client: dir
	$(CC) -g -fPIC -o bin/Test_Client Tests/Test_Client.c -lpodmq_d $(C_FLAGS_D)



