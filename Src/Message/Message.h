/*


 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef PODMQ_MESSAGE_H
#define PODMQ_MESSAGE_H


#include "../PodMQ.h"




#define PODMQ_TYPE_MESSAGE			(podmq_message_get_type())
G_DECLARE_FINAL_TYPE(PodMQMessage, podmq_message, PODMQ, MESSAGE, GObject);



typedef enum 
{
	PODMQ_OP_INVALID	= 0,
	PODMQ_PUT		= 1,
	PODMQ_GET 		= 2,
	PODMQ_CREATE 		= 3,
	PODMQ_PEEK		= 4
} PodMQOperation;




typedef struct 
{
	gchar				*key;
	gchar				*value;
} PodMQHeader;




PODMQ_API 
PodMQMessage *
podmq_message_new(
	const gchar			*queue_name,
	const gchar			*message_body,
	const gchar			*trace_id,
	GArray				*headers,
	PodMQOperation			operation
);




PODMQ_API 
PodMQMessage *
podmq_message_new_from_json(
	const gchar			*json
);




PODMQ_API 
PodMQMessage *
podmq_message_new_from_json_object(
	const JsonObject		*json_object
);




PODMQ_API 
PodMQMessage *
podmq_message_copy(
	PodMQMessage			*to_be_copied
);




PODMQ_API 
const gchar *
podmq_message_get_data_raw(
	PodMQMessage			*self
);




PODMQ_API 
const gchar *
podmq_message_get_data_pretty(
	PodMQMessage			*self
);




PODMQ_API 
JsonNode *
podmq_message_get_data(
	PodMQMessage			*self
);




PODMQ_API 
gint64
podmq_message_get_data_size(
	PodMQMessage			*self
);




PODMQ_API 
gint64
podmq_message_get_header_length(
	PodMQMessage			*self
);




PODMQ_API 
gint64
podmq_message_get_message_length(
	PodMQMessage			*self
);




PODMQ_API 
const gchar *
podmq_message_get_queue_name(
	PodMQMessage			*self
);




PODMQ_API 
const gchar *
podmq_message_get_trace_id(
	PodMQMessage			*self
);




PODMQ_API 
gint64
podmq_message_get_timestamp(
	PodMQMessage			*self
);




PODMQ_API 
GArray *
podmq_message_get_misc_headers(
	PodMQMessage			*self
);




PODMQ_API 
const gchar *
podmq_message_get_message_body(
	PodMQMessage			*self
);




PODMQ_API
PodMQOperation
podmq_message_get_operation(
	PodMQMessage			*self
);




PODMQ_API 
const gchar *
podmq_message_get_header_value(
	PodMQMessage			*self,
	const gchar 			*name
);






#endif