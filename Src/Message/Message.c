/*


 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "Message.h"




static void (*ancestor_dispose)(GObject *) = NULL;
static void (*ancestor_finalize)(GObject *) = NULL;




/* See Examples/Payload.json */
struct _PodMQMessage
{
	GObject				parent;
	JsonNode			*json_message;
	gchar				*raw_data;
	gchar 				*pretty_data;
	gint64				data_size;
	gboolean			created_as_copy;

	struct 
	{
		struct 
		{
			gint64			message_length;
			gchar			*queue_name;
			gchar 			*trace_id;
			gint64			timestamp;
			PodMQOperation		operation;
			GArray			*misc_headers;
		} header;
		gchar			*body;
	} content;
};




G_DEFINE_TYPE(PodMQMessage, podmq_message, G_TYPE_OBJECT);



static 
void 
_podmq_message_create_header(
	PodMQMessage			*self,
	JsonBuilder 			*builder
){
	gint i;

	json_builder_set_member_name(builder, "queue_name");
	json_builder_add_string_value(builder, self->content.header.queue_name);
	
	if (NULL != self->content.header.trace_id)
	{
		json_builder_set_member_name(builder, "trace_id");
		json_builder_add_string_value(builder, self->content.header.trace_id);
	}

	json_builder_set_member_name(builder, "timestamp");
	json_builder_add_int_value(builder, self->content.header.timestamp);
	

	json_builder_set_member_name(builder, "request_type");
	json_builder_add_int_value(builder, (gint64)self->content.header.operation);

	json_builder_set_member_name(builder, "message_length");
	json_builder_add_int_value(builder, self->content.header.message_length);

	if (NULL != self->content.header.misc_headers)
	{
		for (
			i = 0;
			i < (gint)self->content.header.misc_headers->len;
			i++
		){
			PodMQHeader header;

			header = g_array_index(
				self->content.header.misc_headers,
				PodMQHeader,
				i
			);

			if (NULL != header.key && NULL != header.value)
			{
				json_builder_set_member_name(builder, header.key);
				json_builder_add_string_value(builder, header.value);
			}
		}
	}
}




static 
void 
_podmq_message_create_body(
	PodMQMessage			*self,
	JsonBuilder 			*builder
){
	json_builder_set_member_name(builder, "body");
	json_builder_add_string_value(builder, self->content.body);
}




static 
void 
_podmq_message_create_json_message(
	PodMQMessage			*self
){
	JsonGenerator *generator;

	generator = json_generator_new();
	json_generator_set_root(generator, self->json_message);

	if (NULL != self->raw_data)
	{ g_free(self->raw_data); }
	
	self->raw_data = json_generator_to_data(generator, NULL);

	g_object_unref(generator);

	generator = json_generator_new();
	json_generator_set_root(generator, self->json_message);
	json_generator_set_pretty(generator, TRUE);
	json_generator_set_indent(generator, 4);
	self->pretty_data = json_generator_to_data(generator, NULL);

	g_object_unref(generator);
}




static 
void 
_podmq_message_create_message(
	PodMQMessage			*self
){
	JsonBuilder *builder;

	builder = json_builder_new();

	json_builder_begin_object(builder);

	json_builder_set_member_name(builder, "header");	
	json_builder_begin_object(builder);
	_podmq_message_create_header(self, builder);
	json_builder_end_object(builder);

	_podmq_message_create_body(self, builder);

	json_builder_end_object(builder);

	self->json_message = json_builder_get_root(builder);
	g_object_unref(builder);

	_podmq_message_create_json_message(self);
}




static 
void 
_podmq_message_create_message_from_json_foreach_cb(
	JsonObject			*object,
	const gchar			*member_name,
	JsonNode			*member_node,
	gpointer			user_data
){
	PodMQMessage *self;

	self = user_data;
	
	if (0 == g_strcmp0("queue_name", member_name))
	{ self->content.header.queue_name = g_strdup(json_node_get_string(member_node)); }
	else if (0 == g_strcmp0("trace_id", member_name))
	{ self->content.header.trace_id = g_strdup(json_node_get_string(member_node)); }
	else if (0 == g_strcmp0("timestamp", member_name))
	{ self->content.header.timestamp = json_node_get_int(member_node); }
	else if (0 == g_strcmp0("message_length", member_name))
	{ self->content.header.message_length = json_node_get_int(member_node); }
	else if (0 == g_strcmp0("request_type", member_name))
	{ self->content.header.operation = json_node_get_int(member_node); }
	else
	{
		PodMQHeader header;

		if (NULL == self->content.header.misc_headers)
		{ self->content.header.misc_headers = g_array_new(TRUE, TRUE, sizeof(PodMQHeader)); }

		header.key = g_strdup(member_name);
		header.value = g_strdup(json_node_get_string(member_node));

		g_array_append_val(self->content.header.misc_headers, header);
	}
}



static 
void 
_podmq_message_create_message_from_json(
	PodMQMessage			*self,
	const gchar			*msg
){
	JsonParser *parser;
	JsonNode *root_node;
	JsonObject *root;
	JsonObject *header;
	
	parser = json_parser_new();
	json_parser_load_from_data(parser, msg, -1, NULL);
	root_node = json_parser_get_root(parser);
	root = json_node_get_object(root_node);

	self->content.body = g_strdup(json_object_get_string_member(root, "body"));

	header = json_object_get_object_member(root, "header");

	json_object_foreach_member(header, _podmq_message_create_message_from_json_foreach_cb, (gpointer)self);

	g_object_unref(parser);
}



static 
void 
podmq_message_dispose(
	PodMQMessage			*self
){
	g_return_if_fail(PODMQ_IS_MESSAGE(self));

	self = PODMQ_MESSAGE(self);
	ancestor_dispose(G_OBJECT(self));
}




static 
void 
podmq_message_finalize(
	PodMQMessage			*self
){
	g_return_if_fail(PODMQ_IS_MESSAGE(self));
	gint i;

	self = PODMQ_MESSAGE(self);

	json_node_free(self->json_message);
	g_free(self->raw_data);
	g_free(self->pretty_data);
	g_free(self->content.body);
	g_free(self->content.header.queue_name);
	g_free(self->content.header.trace_id);

	/* Since headers is a shallow copy we don't want */
	/* to do a double free. */
	if (FALSE == self->created_as_copy && self->content.header.misc_headers)
	{
		for (
			i = 0;
			i < (gint)self->content.header.misc_headers->len;
			i++
		){
			PodMQHeader header;

			header = g_array_index(
				self->content.header.misc_headers,
				PodMQHeader,
				i
			);

			g_free(header.key);
			g_free(header.value);
		}
	}

	if (NULL != self->content.header.misc_headers)
	{ g_array_free(self->content.header.misc_headers, TRUE); }

	ancestor_finalize(G_OBJECT(self));
}




static 
void 
podmq_message_init(
	PodMQMessage			*self
){ (void)0; }




static 
void 
podmq_message_class_init(
	PodMQMessageClass		*klazz
){
	GObjectClass *ancestor_klazz;

	ancestor_klazz = G_OBJECT_CLASS(klazz);

	ancestor_dispose = ancestor_klazz->dispose;
	ancestor_finalize = ancestor_klazz->finalize;

	ancestor_klazz->dispose = (void (*)(GObject *))podmq_message_dispose;
	ancestor_klazz->finalize = (void (*)(GObject *))podmq_message_finalize;
}




PODMQ_API 
PodMQMessage *
podmq_message_new(
	const gchar			*queue_name,
	const gchar			*message_body,
	const gchar			*trace_id,
	GArray				*headers,
	PodMQOperation			operation
){
	PodMQMessage *self;

	g_return_val_if_fail(queue_name, NULL);
	g_return_val_if_fail(message_body, NULL);

	self = g_object_new(PODMQ_TYPE_MESSAGE, NULL);

	self->content.header.queue_name = g_strdup(queue_name);
	self->content.body = g_strdup(message_body);
	self->content.header.misc_headers = headers;
	self->content.header.operation = operation;
	self->content.header.message_length = strlen(message_body);
	self->created_as_copy = FALSE;

	if (NULL != trace_id)
	{ self->content.header.trace_id = g_strdup(trace_id); }

	self->content.header.timestamp = g_get_real_time();

	_podmq_message_create_message(self);

	return PODMQ_MESSAGE(self); 
}




PODMQ_API 
PodMQMessage *
podmq_message_new_from_json(
	const gchar			*json
){
	g_return_val_if_fail(json, NULL);

	PodMQMessage *self;

	self = g_object_new(PODMQ_TYPE_MESSAGE, NULL);

	self->created_as_copy = FALSE;
	_podmq_message_create_message_from_json(self, json);
	_podmq_message_create_message(self);

	return PODMQ_MESSAGE(self);
}




PODMQ_API 
PodMQMessage *
podmq_message_new_from_json_object(
	const JsonObject		*json_object
){
	g_return_val_if_fail(json_object, NULL);

	g_autoptr(JsonGenerator) generator;
	g_autoptr(JsonNode) root_node;
	g_autofree gchar *raw_json;

	generator = json_generator_new();
	root_node = json_node_new(JSON_NODE_OBJECT);

	json_node_set_object(root_node, json_object);
	json_generator_set_root(generator, root_node);
	raw_json = json_generator_to_data(generator, NULL);

	return podmq_message_new_from_json((const gchar *)raw_json);
}




PODMQ_API 
PodMQMessage *
podmq_message_copy(
	PodMQMessage			*to_be_copied
){
	g_return_val_if_fail(PODMQ_IS_MESSAGE(to_be_copied), NULL);

	PodMQMessage *self;

	self = g_object_new(PODMQ_TYPE_MESSAGE, NULL);

	self->content.body = g_strdup(to_be_copied->content.body);
	self->content.header.message_length = to_be_copied->content.header.message_length;

	if (NULL != to_be_copied->content.header.misc_headers)
	{ self->content.header.misc_headers = g_array_copy(to_be_copied->content.header.misc_headers); }
	
	self->content.header.operation = to_be_copied->content.header.operation;
	self->content.header.queue_name = g_strdup(to_be_copied->content.header.queue_name);
	self->content.header.timestamp = to_be_copied->content.header.timestamp;
	self->content.header.trace_id = g_strdup(to_be_copied->content.header.trace_id);
	self->data_size = to_be_copied->data_size;
	self->raw_data = g_strdup(to_be_copied->raw_data);
	self->created_as_copy = TRUE;

	_podmq_message_create_message(self);

	return self;
}




PODMQ_API 
const gchar *
podmq_message_get_data_raw(
	PodMQMessage			*self
){
	g_return_val_if_fail(PODMQ_IS_MESSAGE(self), NULL);
	return self->raw_data;
}




PODMQ_API 
const gchar *
podmq_message_get_data_pretty(
	PodMQMessage			*self
){
	g_return_val_if_fail(PODMQ_IS_MESSAGE(self), NULL);
	return self->pretty_data;
}




PODMQ_API 
JsonNode *
podmq_message_get_data(
	PodMQMessage			*self
){
	g_return_val_if_fail(PODMQ_IS_MESSAGE(self), NULL);
	return self->json_message;
}




PODMQ_API 
gint64
podmq_message_get_data_size(
	PodMQMessage			*self
){
	g_return_val_if_fail(PODMQ_IS_MESSAGE(self), -1);
	return self->data_size;
}




PODMQ_API 
gint64
podmq_message_get_header_length(
	PodMQMessage			*self
){
	g_return_val_if_fail(PODMQ_IS_MESSAGE(self), -1);

	if (NULL != self->content.header.misc_headers)
	{ return self->content.header.misc_headers->len; }

	return 0;
}




PODMQ_API 
gint64
podmq_message_get_message_length(
	PodMQMessage			*self
){
	g_return_val_if_fail(PODMQ_IS_MESSAGE(self), -1);
	return self->content.header.message_length;
}




PODMQ_API 
const gchar *
podmq_message_get_queue_name(
	PodMQMessage			*self
){
	g_return_val_if_fail(PODMQ_IS_MESSAGE(self), NULL);
	return self->content.header.queue_name;
}




PODMQ_API 
const gchar *
podmq_message_get_trace_id(
	PodMQMessage			*self
){
	g_return_val_if_fail(PODMQ_IS_MESSAGE(self), NULL);
	return self->content.header.trace_id;
}




PODMQ_API 
gint64
podmq_message_get_timestamp(
	PodMQMessage			*self
){
	g_return_val_if_fail(PODMQ_IS_MESSAGE(self), (guint64)-1);
	return self->content.header.timestamp;
}




PODMQ_API 
GArray *
podmq_message_get_misc_headers(
	PodMQMessage			*self
){
	g_return_val_if_fail(PODMQ_IS_MESSAGE(self), NULL);
	return self->content.header.misc_headers;
}




PODMQ_API 
const gchar *
podmq_message_get_message_body(
	PodMQMessage			*self
){
	g_return_val_if_fail(PODMQ_IS_MESSAGE(self), NULL);
	return self->content.body;
}




PODMQ_API
PodMQOperation
podmq_message_get_operation(
	PodMQMessage			*self
){
	g_return_val_if_fail(PODMQ_IS_MESSAGE(self), PODMQ_OP_INVALID);
	return self->content.header.operation;
}




PODMQ_API 
const gchar *
podmq_message_get_header_value(
	PodMQMessage			*self,
	const gchar 			*name
){
	g_return_val_if_fail(PODMQ_IS_MESSAGE(self), NULL);
	g_return_val_if_fail(name, NULL);

	gint i;

	for (
		i = 0;
		i < (gint)self->content.header.misc_headers->len;
		i++
	){
		PodMQHeader current;

		current = g_array_index(self->content.header.misc_headers, PodMQHeader, i);
		if (0 == g_strcmp0(name, current.key))
		{ return (const gchar *)current.value; }
	}

	return NULL;
}