/*


 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "../PodMQ.h"
#include "../Client/Client.h"
#include "../Server/Server.h"



typedef struct 
{
	gint			*argc;
	gchar			***argv;

	PodMQClient		*client;
	GArray 			*parsed_headers;

	gboolean		server_mode;

	gboolean		print_help;
	gchar 			*queue_name;
	gchar 			*queue_manager;
	gchar 			*api_token;
	gchar 			*server_name;
	gint 			server_port;
	gboolean		http_secure;

	gboolean 		create;
	gchar 			**put_data;
	gboolean 		get;
	gboolean 		peek;
	gboolean		get_queues;
	gboolean		get_summary;

	gboolean 		json_output;
	gboolean 		json_pretty;

	gchar 			**headers;
	gboolean 		stay_attached;
	gint64 			poll_sec;
	gboolean 		dry_run;
	gchar 			*trace_id;

	GOptionContext		*option_context;
} ProgramState;




static 
void 
podmq_standalone_print_help_and_exit(
	ProgramState		*state,
	gint			exit_code
){
	gchar *help;

	help = g_option_context_get_help(state->option_context, TRUE, NULL);
	g_print("%s", help);

	g_free(help);
	exit(exit_code);
}



static 
void
podmq_standalone_parse_headers(
	ProgramState		*state
){
	gchar **copy;
	gchar *data;
	gchar *key;
	gchar *value;
	PodMQHeader header;
	guint i;

	if (NULL == state->headers)
	{ return; }

	state->parsed_headers = g_array_new(TRUE, TRUE, sizeof(PodMQHeader));
	copy = g_strdupv(state->headers);

	for (
		i = 0;
		i < g_strv_length(copy);
		i++
	){
		data = copy[i];
		key = data;
		value = g_strrstr(data, ":");

		if (NULL != value)
		{
			*value = '\0';
			value = (gchar *)(void *)((gsize)value + 0x01);

			while (' ' == *value)
			{ value = (gchar *)(void *)((gsize)value + 0x01); }

			memset(&header, 0x00, sizeof(PodMQHeader));
			header.key = g_strdup(key);
			header.value = g_strdup(value);

			g_array_append_val(state->parsed_headers, header);
		}
	}

	g_strfreev(copy);
}



static 
void 
podmq_standalone_parse_args(
	ProgramState		*state
){
	g_return_if_fail(state);
	gboolean parse_status;
	
	static gboolean server_mode;

	static gchar *queue_name;
	static gchar *queue_manager;
	static gchar *api_token;
	static gchar *server_name;
	static gint server_port;
	static gboolean http_secure;

	static gboolean create;
	static gchar **put_data;
	static gboolean get;
	static gboolean peek;
	static gboolean get_queues;
	static gboolean get_summary;
	
	static gboolean json_output;
	static gboolean json_pretty;

	static gchar **headers;
	static gboolean stay_attached;
	static gint64 poll_sec;
	static gboolean dry_run;
	static gchar *trace_id;

	state->option_context = g_option_context_new("- PodMQ Standlone Utility");
	state->poll_sec = 3;

	static GOptionEntry option_entries[] = 
	{
		/* Mode */
		{ "server-mode", 	'S', 0, G_OPTION_ARG_NONE,		&server_mode, 	"Run as a server (default is client)",		NULL			},

		/* Server Settings */
		{ "queue", 		'q', 0, G_OPTION_ARG_STRING, 		&queue_name, 	"queue_name to connect to", 			"queue_name"		},
		{ "manager", 		'm', 0, G_OPTION_ARG_STRING, 		&queue_manager, "queue_manager to interact with", 		"queue_manager"		},
		{ "api-token", 		'a', 0, G_OPTION_ARG_STRING, 		&api_token, 	"api_token to interact with", 			"api_token"		},
		{ "server", 		's', 0, G_OPTION_ARG_STRING, 		&server_name, 	"server_name to connect to (can be an IP)", 	"server_name"		},
		{ "port", 		'P', 0, G_OPTION_ARG_INT, 		&server_port, 	"port to connect to server_name on (TCP)", 	"port"			},
		{ "http-secure",	0x0, 0, G_OPTION_ARG_NONE,		&http_secure,	"use https for connection", 			NULL			},

		/* Queue Operations */
		{ "create", 		'c', 0, G_OPTION_ARG_NONE,		&create,	"create queue_name (requires -q)",		NULL			},
		{ "put",		'p', 0, G_OPTION_ARG_STRING_ARRAY,	&put_data,	"put_data to send to queue (put)",		"put_data"		},
		{ "get", 		'g', 0, G_OPTION_ARG_NONE,		&get,		"get next message from the queue",		NULL			},
		{ "peek",		'G', 0, G_OPTION_ARG_NONE,		&peek,		"peek the entire queue", 			NULL			},
		{ "get-all-queues",	'a', 0, G_OPTION_ARG_NONE, 		&get_queues,	"get all queues for the respective manager", 	NULL			},
		{ "get-summary",	'M', 0, G_OPTION_ARG_NONE,		&get_summary,	"get summary for the respective manager", 	NULL			},

		/* Output Modifiers */
		{ "json",		'j', 0, G_OPTION_ARG_NONE, 		&json_output,	"print result in json",				NULL			},
		{ "json-pretty",	'J', 0, G_OPTION_ARG_NONE,		&json_pretty,	"print result in json (pretty format)",		NULL			},

		/* Operation Modifers */
		{ "header",		'H', 0, G_OPTION_ARG_STRING_ARRAY,	&headers,	"attach header to the request",			"header_key_value"	},
		{ "stay-attached", 	'A', 0, G_OPTION_ARG_NONE, 		&stay_attached, "stay attached and poll for new items", 	NULL			},
		{ "poll-sec", 		0x0, 0, G_OPTION_ARG_INT64, 		&poll_sec, 	"poll queue every number of poll_sec seconds", 	"poll_sec"		},
		{ "dry-run",		'D', 0, G_OPTION_ARG_NONE,		&dry_run,	"perform a dry run (don't actually send data)",	NULL			},
		{ "trace-id",		'T', 0, G_OPTION_ARG_STRING,		&trace_id, 	"set the trace_id for the transaction",		"trace_id"		}
	};

	g_option_context_add_main_entries(state->option_context, option_entries, NULL);
	parse_status = g_option_context_parse(state->option_context, state->argc, state->argv, NULL);

	if (FALSE == parse_status)
	{ podmq_standalone_print_help_and_exit(state, 1); }

	state->server_mode = server_mode;

	state->queue_name = g_strdup(queue_name);
	state->queue_manager = g_strdup(queue_manager);
	state->api_token = g_strdup(api_token);
	state->server_name = g_strdup(server_name);
	state->server_port = server_port;
	state->http_secure = http_secure;

	state->create = create;
	state->put_data = g_strdupv(put_data);
	state->get = get;
	state->peek = peek;
	state->get_queues = get_queues;
	state->get_summary = get_summary;

	state->json_output = json_output;
	state->json_pretty = json_pretty;

	state->headers = g_strdupv(headers);
	state->stay_attached = stay_attached;
	state->poll_sec = poll_sec;
	state->dry_run = dry_run;
	state->trace_id = g_strdup(trace_id);
}




/* Never returns */
static 
void 
podmq_standalone_init_server(
	ProgramState 		*state
){
	g_return_if_fail(state);

	if (FALSE == state->server_mode)
	{ return; }

	g_autoptr(PodMQServer) server;
	g_autoptr(GArray) tokens;
	g_autoptr(PodMQQueueManager) manager;
	g_autoptr(PodMQQueue) queue;

	server = NULL;
	tokens = NULL;

	if (NULL != state->api_token)
	{
		tokens = g_array_new(TRUE, TRUE, sizeof(const gchar *));
		g_array_append_val(tokens, state->api_token);
	}

	if (NULL == state->server_name)
	{ state->server_name = g_strdup("127.0.0.1"); }

	if (0 >= state->server_port)
	{ state->server_port = 9001; }

	server = podmq_server_new(
		state->server_name,
		(guint16)state->server_port,
		tokens
	);

	manager = podmq_queue_manager_new((state->queue_manager) ? state->queue_manager : "default");
	queue = podmq_queue_new((state->queue_name) ? state->queue_name : "default");

	podmq_queue_manager_add(manager, queue);
	podmq_server_register_manager(server, manager);
	podmq_server_set_default_manager(server, manager);

	podmq_server_serve(server);
	
	exit(0);
}




static 
void 
podmq_standalone_init_client(
	ProgramState		*state
){
	g_return_if_fail(state);

	if (NULL == state->server_name)
	{ state->server_name = g_strdup("localhost"); }

	if (0 >= state->server_port)
	{ state->server_port = 9001; }

	state->client = podmq_client_new(
		state->server_name,
		(guint16)state->server_port,
		state->queue_manager,
		state->queue_name,
		state->api_token,
		state->http_secure
	);

	if (NULL == state->client)
	{
		g_printerr("Could not create PodMQ Client\n");
		exit(2);
	}
}




static 
ProgramState *
podmq_standalone_init(
	gint			*argc,
	gchar			***argv
){
	ProgramState *state;

	state = g_malloc0(sizeof(ProgramState));
	g_return_val_if_fail(state, NULL);


	state->argc = argc;
	state->argv = argv;
	state->print_help = (1 == *argc) ? TRUE : FALSE;

	podmq_standalone_parse_args(state);
	podmq_standalone_parse_headers(state);

	if (state->print_help)
	{ podmq_standalone_print_help_and_exit(state, 1); }
	
	if (NULL == state->queue_name)
	{ state->queue_name = g_strdup("default"); }

	if (NULL == state->queue_manager)
	{ state->queue_manager = g_strdup("default"); }

	podmq_standalone_init_server(state);
	podmq_standalone_init_client(state);


	return state;
}



static 
void 
podmq_standalone_finalize(
	ProgramState		*state
){
	g_return_if_fail(state);
	guint i;

	g_object_unref(state->client);
	g_option_context_free(state->option_context);

	g_free(state->api_token);
	g_free(state->queue_manager);
	g_free(state->queue_name);
	g_free(state->server_name);
	g_free(state->trace_id);
	
	g_strfreev(state->put_data);
	g_strfreev(state->headers);

	if (NULL != state->parsed_headers)
	{
		for (
			i = 0;
			i < state->parsed_headers->len;
			i++
		){
			PodMQHeader header;

			memset(&header, 0x00, sizeof(PodMQHeader));
			header = g_array_index(state->parsed_headers, PodMQHeader, i);

			g_free(header.key);
			g_free(header.value);
		}

		g_array_free(state->parsed_headers, FALSE);
	}

	g_free(state);
}




static 
void 
podmq_standalone_create(
	ProgramState		*state
){
	g_return_if_fail(state);
	g_return_if_fail(PODMQ_IS_CLIENT(state->client));

	if (FALSE == state->create || FALSE == state->queue_name)
	{ return; }

	podmq_client_create(
		state->client,
		state->queue_name
	);
}




static 
void 
podmq_standalone_get(
	ProgramState		*state
){
	g_return_if_fail(state);
	g_return_if_fail(PODMQ_IS_CLIENT(state->client));

	if (FALSE == state->get || state->dry_run)
	{ return; }

	g_autoptr(PodMQMessage) msg;
	gchar *data;

	msg = NULL;
	data = NULL;

	msg = podmq_client_get(state->client);
	if (NULL == msg || FALSE == PODMQ_IS_MESSAGE(msg))
	{ return; }

	if (state->json_pretty)
	{ data = (gchar *)podmq_message_get_data_pretty(msg); }
	else if (state->json_output)
	{ data = (gchar *)podmq_message_get_data_raw(msg); }
	else 
	{ data = (gchar *)podmq_message_get_message_body(msg); }

	g_print("%s\n", data);
}



static 
void 
podmq_standalone_put(
	ProgramState		*state
){
	g_return_if_fail(state);
	g_return_if_fail(PODMQ_IS_CLIENT(state->client));

	if (NULL == state->put_data || NULL == state->put_data[0] || '\0' == *(state->put_data[0]))
	{ return; }

	PodMQMessage *msg;
	GArray *messages;
	gchar *data;
	guint i;

	messages = g_array_new(TRUE, TRUE, sizeof(PodMQMessage *));

	for (
		i = 0, data = state->put_data[0];
		data != NULL && '\0' != *(state->put_data[i]);
		i++, data = state->put_data[i]
	){
		msg = podmq_message_new(
			(state->queue_name) ? state->queue_name : "default",
			(const gchar *)data,
			NULL,
			g_array_copy(state->parsed_headers),
			PODMQ_PUT
		);

		g_array_append_val(messages, msg);
	}

	podmq_client_put_many(state->client, messages);

	for (
		i = 0;
		i < messages->len;
		i++
	){ g_object_unref(g_array_index(messages, PodMQMessage *, i)); }

	g_array_free(messages, TRUE);
}



static 
void 
podmq_standalone_peek(
	ProgramState		*state
){
	g_return_if_fail(state);
	g_return_if_fail(PODMQ_IS_CLIENT(state->client));

	if (FALSE == state->peek)
	{ return; }

	g_autoptr(GArray) messages;
	PodMQMessage *msg;
	gchar *data;
	gint i;

	messages = podmq_client_peek(state->client);
	if (NULL == messages || 0 == messages->len)
	{ return; }


	for (
		i = 0;
		i < (gint)messages->len;
		i++
	){
		msg = g_array_index(messages, PodMQMessage *, i);

		if (state->json_pretty)
		{ data = (gchar *)podmq_message_get_data_pretty(msg); }
		else if (state->json_output)
		{ data = (gchar *)podmq_message_get_data_raw(msg); }
		else
		{ data = (gchar *)podmq_message_get_message_body(msg); }

		g_print("%s\n", data);
		g_object_unref(msg);
	}
}



static 
void 
podmq_standalone_get_all_queue_names(
	ProgramState		*state
){
	g_return_if_fail(state);
	g_return_if_fail(PODMQ_IS_CLIENT(state->client));

	if (FALSE == state->get_queues)
	{ return; }

	g_autoptr(GArray) queues;
	gint i;
	gchar *name;

	queues = podmq_client_get_all_queue_names(state->client);
	
	if (state->json_pretty)
	{ g_print("[\n"); }
	else if (state->json_output)
	{ g_print("["); }

	for (
		i = 0;
		i < (gint)queues->len;
		i++
	){
		name = g_array_index(queues, gchar *, i);

		if (state->json_pretty)
		{ g_print("    \"%s\"%s", name, (i == (gint)queues->len - 1) ? "\n" : ",\n"); }
		else if (state->json_output)
		{ g_print("\"%s\"%s", name, (i == (gint)queues->len - 1) ? "" : ","); }
		else 
		{ g_print("%s\n", name); }
	}
	
	if (state->json_pretty)
	{ g_print("]\n"); }
	else if (state->json_output)
	{ g_print("]\n"); }
}




static 
void 
podmq_standalone_get_manager_summary(
	ProgramState		*state
){
	g_return_if_fail(state);
	g_return_if_fail(PODMQ_IS_CLIENT(state->client));

	if (FALSE == state->get_summary)
	{ return; }

	g_autoptr(GArray) queues;
	gint i;
	PodMQQueueSummary summary;

	queues = podmq_client_get_manager_summary(state->client);

	if (state->json_pretty)
	{ g_print("[\n"); }
	else if (state->json_output)
	{ g_print("["); }

	for (
		i = 0;
		i < (gint)queues->len;
		i++
	){
		summary = g_array_index(queues, PodMQQueueSummary, i);

		if (state->json_pretty)
		{ 
			g_print(
				"    {\n        \"queue_name\": \"%s\",\n        \"count\": %d\n    }%s", 
				summary.queue_name,
				summary.item_count,
				(i == (gint)queues->len - 1) ? "\n" : ",\n"
			);
		}
		else if (state->json_output)
		{ 
			g_print(
				"{\"queue_name\":\"%s\",\"count\":%d}%s", 
				summary.queue_name,
				summary.item_count,
				(i == (gint)queues->len - 1) ? "" : ","
			);
		}
		else 
		{ g_print("%s : %d\n", summary.queue_name, summary.item_count); }
	}

	if (state->json_pretty)
	{ g_print("]\n"); }
	else if (state->json_output)
	{ g_print("]"); }
}




gint 
main(
	gint			argc,
	gchar			**argv
){
	ProgramState *state;

	state = podmq_standalone_init(&argc, &argv);
	
	podmq_standalone_create(state);

	do
	{
		podmq_standalone_get(state);
		podmq_standalone_put(state);
		podmq_standalone_peek(state);
		podmq_standalone_get_all_queue_names(state);
		podmq_standalone_get_manager_summary(state);
	} while (state->stay_attached && !sleep(state->poll_sec));

	podmq_standalone_finalize(state);
	return 0;
}