"""

 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


from .PodMQMessage import PodMQMessage
import requests 
import json


class PodMQClient(object):
	def __init__(self, host="127.0.0.1", port=9001, manager="default", queue="default", api_token="", http_secure=False):
		self.host = host
		self.port = port
		self.manager = manager
		self.queue = queue 
		self.api_token = api_token
		self.http_secure = http_secure

		proto = "http"
		
		if (http_secure == True):
			proto = "https"

		self.url_get = "%s://%s:%d/api/v1/get?manager=%s&queue=%s&api-token=%s" % (proto, host, port, manager, queue, api_token)
		self.url_put = "%s://%s:%d/api/v1/put?manager=%s&queue=%s&api-token=%s" % (proto, host, port, manager, queue, api_token)
		self.url_put_many = "%s://%s:%d/api/v1/put_many?manager=%s&queue=%s&api-token=%s" % (proto, host, port, manager, queue, api_token)
		self.url_create = "%s://%s:%d/api/v1/create?manager=%s&queue=%s&api-token=%s" % (proto, host, port, manager, queue, api_token)
		self.url_peek = "%s://%s:%d/api/v1/peek?manager=%s&queue=%s&api-token=%s" % (proto, host, port, manager, queue, api_token)
		self.url_get_all_queue_names = "%s://%s:%d/api/v1/get_all_queue_names?manager=%s&api-token=%s" % (proto, host, port, manager, api_token)
		self.url_get_manager_summary = "%s://%s:%d/api/v1/get_manager_summary?manager=%s&api-token=%s" % (proto, host, port, manager, api_token)



	def get(self) -> PodMQMessage:
		r = requests.get(self.url_get)
		if (r.text != ""):
			json_obj = json.loads(r.text)
			return PodMQMessage(json_payload=json_obj)
		
		return None


	def put(self, message: PodMQMessage) -> None:
		json_obj = message.get_data_raw()
		r = requests.post(self.url_put, data=json_obj)

	
	def put_many(self, messages: list) -> None:
		json_array = []
		for message in messages:
			json_array.append(message.json_payload)

		r= requests.post(self.url_put_many, data=json.dumps(json_array))


	def create(self) -> None:
		r = requests.get(self.url_create)

	
	def get_all_queue_names(self) -> list[dict]:
		r = requests.get(self.url_get_all_queue_names)
		return json.loads(r.text)


	def get_manager_summary(self) -> list[dict]:
		r = requests.get(self.url_get_manager_summary)
		return json.loads(r.text)


	def peek(self) -> list[PodMQMessage]:
		r = requests.get(self.url_peek)
		resp_list = []

		messages = json.loads(r.text)

		for message in messages:
			resp_list.append(PodMQMessage(json_payload=message))

		return resp_list
		