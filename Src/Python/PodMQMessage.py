"""

 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


from time import time
import json 


class PodMQMessage(object):
	def __init__(self, queue_name="default", message_body="", trace_id="", headers={}, operation=0, json_payload={}):
		if ({} != json_payload):
			self.headers = {}
			self.message_body = json_payload['body']
			self.message_length = len(self.message_body)
			self.json_payload = json_payload

			for header in json_payload['header']:
				if (header == "queue_name"):
					self.queue_name = json_payload['header'][header]
				elif (header == "trace_id"):
					self.trace_id = json_payload['header'][header]
				elif (header == "timestamp"):
					self.timestamp = int(json_payload['header'][header])
				elif (header == "message_length"):
					self.message_length = json_payload['header'][header]
				elif (header == "request_type"):
					self.request_type = int(json_payload['header'][header])
				else:
					self.headers[header] = json_payload['header'][header]
			
			self.message_length = len(self.message_body)

		elif (message_body == ""):
			raise Exception("PodMQMessage was attempted to be init with null values for everything")

		else:
			self.queue_name = queue_name
			self.message_body = message_body
			self.message_length = len(message_body)
			self.trace_id = trace_id
			self.headers = headers
			self.operation = operation
			self.headers = headers

			self.json_payload = {}
			self.json_payload['header'] = {
				"queue_name" : self.queue_name,
				"trace_id" : self.trace_id,
				"timestamp" : int(time()*1000000),
				"message_length" : len(self.message_body),
				"request_type" : self.operation
			}
			self.json_payload['body'] = self.message_body

			# Other headers
			if ({} != headers):
				for header in headers:
					self.json_payload['header'][header] = headers[header]

	def copy(self):
		return PodMQMessage(json_payload=self.json_payload)


	def get_data_raw(self) -> str:
		return json.dumps(self.json_payload)

	
	def get_data_pretty(self) -> str:
		return json.dumps(self.json_payload, indent=4)

	
	def get_data(self) -> dict:
		return self.json_payload


	def get_data_size(self) -> int:
		return self.message_length


	def get_header_length(self) -> int:
		return len(json.dumps(self.headers))


	def get_message_length(self) -> int:
		return self.message_length


	def get_queue_name(self) -> str:
		return self.queue_name


	def get_trace_id(self) -> str:
		return self.trace_id


	def get_misc_headers(self) -> dict:
		return self.headers


	def get_timestamp(self) -> int:
		return self.timestamp


	def get_message_body(self) -> str:
		return self.message_body

	
	def get_operation(self) -> int:
		return self.operation


	def get_header_value(self, header: str) -> str:
		if (header == "queue_name"):
			return self.queue_name
		elif (header == "trace_id"):
			return self.trace_id
		elif (header == "timestamp"):
			return self.timestamp
		elif (header == "message_length"):
			return self.message_length
		elif (header == "request_type"):
			return self.request_type
		else:
			return self.headers[header]
			