/*


 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "Client.h"




static void (*ancestor_dispose)(GObject *) = NULL;
static void (*ancestor_finalize)(GObject *) = NULL;




struct _PodMQClient
{
	GObject				parent;
	gchar				*host;
	guint16				port;
	gchar 				*manager;
	gchar 				*queue;
	gchar 				*api_token;
	gboolean			http_secure;

	gchar				*url_get;
	gchar				*url_put;
	gchar				*url_put_many;
	gchar 				*url_create;
	gchar				*url_peek;
	gchar 				*url_get_all_queue_names;
	gchar 				*url_get_manager_summary;
};




G_DEFINE_TYPE(PodMQClient, podmq_client, G_TYPE_OBJECT);




static 
GBytes *
_podmq_client_api_caller(
	const gchar 			*url,
	const gchar 			*payload,
	const gchar 			*method
){
	g_return_val_if_fail(url, NULL);
	g_return_val_if_fail(method, NULL);

	g_autoptr(SoupSession) session;
	g_autoptr(SoupMessage) msg;
	g_autoptr(GError) err;
	g_autoptr(GBytes) req;
	GBytes *bytes;

	session = soup_session_new();
	err = NULL;
	req = NULL;
	msg = NULL;

	msg = soup_message_new(
		method,
		url
	);

	if (NULL != payload)
	{ 
		req = g_bytes_new(
			payload,
			strlen(payload)
		);

		soup_message_set_request_body_from_bytes(
			msg,
			"application/json",
			req
		);
	}

	bytes = soup_session_send_and_read(
		session,
		msg,
		NULL,
		&err
	);

	return bytes;
}




static 
void 
podmq_client_dispose(
	PodMQClient			*self
){
	g_return_if_fail(PODMQ_IS_CLIENT(self));

	self = PODMQ_CLIENT(self);
	ancestor_dispose(G_OBJECT(self));
}




static 
void 
podmq_client_finalize(
	PodMQClient			*self
){
	g_return_if_fail(PODMQ_IS_CLIENT(self));

	self = PODMQ_CLIENT(self);

	g_free(self->api_token);
	g_free(self->host);
	g_free(self->manager);
	g_free(self->queue);
	g_free(self->url_create);
	g_free(self->url_get);
	g_free(self->url_get_all_queue_names);
	g_free(self->url_get_manager_summary);
	g_free(self->url_peek);
	g_free(self->url_put);
	g_free(self->url_put_many);
	
	ancestor_finalize(G_OBJECT(self));
}




static 
void 
podmq_client_init(
	PodMQClient			*self
){ (void)0; }




static 
void 
podmq_client_class_init(
	PodMQClientClass		*klazz
){
	GObjectClass *ancestor_klazz;
	
	ancestor_klazz = G_OBJECT_CLASS(klazz);

	ancestor_dispose = ancestor_klazz->dispose;
	ancestor_finalize = ancestor_klazz->finalize;

	ancestor_klazz->dispose = (void (*)(GObject *))podmq_client_dispose;
	ancestor_klazz->finalize = (void (*)(GObject *))podmq_client_finalize;
}




PODMQ_API
PodMQClient *
podmq_client_new(
	const gchar			*host,
	guint16				port,
	const gchar			*manager,
	const gchar			*queue,
	const gchar			*api_token,
	gboolean 			http_secure
){
	g_return_val_if_fail(host, NULL);

	PodMQClient *self;

	if (0 == port)
	{ port = 9001; }

	self = g_object_new(PODMQ_TYPE_CLIENT, NULL);

	self->host = g_strdup(host);
	self->port = port;
	self->manager = g_strdup(manager);
	self->queue = g_strdup(queue);
	self->api_token = g_strdup(api_token);
	self->http_secure = http_secure;
	
	self->url_get = g_strdup_printf(
		"%s://%s:%d/api/v1/get?manager=%s&queue=%s&api-token=%s",
		(self->http_secure) ? "https" : "http",
		self->host,
		(gint)self->port,
		self->manager,
		self->queue,
		self->api_token
	);
	
	self->url_put = g_strdup_printf(
		"%s://%s:%d/api/v1/put?manager=%s&queue=%s&api-token=%s",
		(self->http_secure) ? "https" : "http",
		self->host,
		(gint)self->port,
		self->manager,
		self->queue,
		self->api_token
	);
	
	self->url_put_many = g_strdup_printf(
		"%s://%s:%d/api/v1/put_many?manager=%s&queue=%s&api-token=%s",
		(self->http_secure) ? "https" : "http",
		self->host,
		(gint)self->port,
		self->manager,
		self->queue,
		self->api_token
	);

	self->url_create = g_strdup_printf(
		"%s://%s:%d/api/v1/create?manager=%s&queue=%s&api-token=%s",
		(self->http_secure) ? "https" : "http",
		self->host,
		(gint)self->port,
		self->manager,
		self->queue,
		self->api_token
	);

	self->url_peek = g_strdup_printf(
		"%s://%s:%d/api/v1/peek?manager=%s&queue=%s&api-token=%s",
		(self->http_secure) ? "https" : "http",
		self->host,
		(gint)self->port,
		self->manager,
		self->queue,
		self->api_token
	);

	self->url_get_all_queue_names = g_strdup_printf(
		"%s://%s:%d/api/v1/get_all_queue_names?manager=%s&api-token=%s",
		(self->http_secure) ? "https" : "http",
		self->host,
		(gint)self->port,
		self->manager,
		self->api_token
	);

	self->url_get_manager_summary = g_strdup_printf(
		"%s://%s:%d/api/v1/get_manager_summary?manager=%s&api-token=%s",
		(self->http_secure) ? "https" : "http",
		self->host,
		(gint)self->port,
		self->manager,
		self->api_token
	);
	


	return self;
}




PODMQ_API 
PodMQMessage *
podmq_client_get(
	PodMQClient			*self
){
	g_return_val_if_fail(PODMQ_IS_CLIENT(self), NULL);

	g_autoptr(GBytes) bytes;
	PodMQMessage *ret;
	gchar *data;

	ret = NULL;
	bytes = _podmq_client_api_caller(
		self->url_get,
		NULL,
		SOUP_METHOD_GET
	);

	if (NULL != bytes)
	{
		data = g_bytes_get_data(bytes, NULL);
		if (NULL != data)
		{ ret = podmq_message_new_from_json(data); }
	}

	return ret; 
}




PODMQ_API 
gboolean
podmq_client_put(
	PodMQClient			*self,
	PodMQMessage			*message
){
	g_return_val_if_fail(PODMQ_IS_CLIENT(self), FALSE);
	g_return_val_if_fail(PODMQ_IS_MESSAGE(message), FALSE);

	g_autoptr(GBytes) ret;

	ret = _podmq_client_api_caller(
		self->url_put,
		podmq_message_get_data_raw(message),
		SOUP_METHOD_POST
	);

	return (NULL != ret) ? TRUE : FALSE;
}




PODMQ_API 
gboolean
podmq_client_put_many(
	PodMQClient			*self,
	GArray				*messages
){
	g_return_val_if_fail(PODMQ_IS_CLIENT(self), FALSE);
	g_return_val_if_fail(messages, FALSE);

	g_autoptr(GString) payload;
	g_autoptr(GBytes) ret;
	PodMQMessage *msg;
	guint i;
	
	payload = g_string_new("[");

	for (
		i = 0;
		i < messages->len;
		i++
	){
		msg = g_array_index(messages, PodMQMessage *, i);

		if (NULL != msg && i != messages->len -1)
		{ g_string_append_printf(payload, "%s,", podmq_message_get_data_raw(msg)); }
		else if (NULL != msg)
		{ g_string_append_printf(payload, "%s", podmq_message_get_data_raw(msg)); }
	}

	g_string_append(payload, "]");

	ret = _podmq_client_api_caller(
		self->url_put_many,
		payload->str,
		SOUP_METHOD_POST
	);

	return (NULL != ret) ? TRUE : FALSE;
}




PODMQ_API 
gboolean
podmq_client_create(
	PodMQClient			*self,
	const gchar 			*name
){
	g_return_val_if_fail(PODMQ_IS_CLIENT(self), FALSE);
	g_return_val_if_fail(name, FALSE);

	g_autoptr(GBytes) ret;

	ret = _podmq_client_api_caller(
		self->url_create,
		NULL,
		SOUP_METHOD_GET
	);

	return (NULL != ret) ? TRUE : FALSE;
}




static
void 
_podmq_client_get_all_queue_names_foreach_cb(
	JsonArray			*array,
	guint				index,
	JsonNode			*element_node,
	gpointer			user_data
){
	GArray *data;
	gchar *name_copy;

	data = user_data;
	name_copy = g_strdup(json_node_get_string(element_node));
	g_array_append_val(data, name_copy);
}




PODMQ_API 
GArray *
podmq_client_get_all_queue_names(
	PodMQClient			*self
){
	g_return_val_if_fail(PODMQ_IS_CLIENT(self), NULL);

	GArray *ret;
	gchar *payload_data;
	g_autoptr(GBytes) resp;
	g_autoptr(JsonParser) parser;
	JsonNode *root_node;
	JsonObject *root;
	JsonArray *queues;

	resp = NULL;
	ret = NULL;
	parser = NULL;

	resp = _podmq_client_api_caller(
		self->url_get_all_queue_names,
		NULL,
		SOUP_METHOD_GET
	);

	g_return_val_if_fail(resp, NULL);

	ret = g_array_new(TRUE, TRUE, sizeof(gchar *));
	payload_data = (gchar *)g_bytes_get_data(resp, NULL);

	parser = json_parser_new();
	json_parser_load_from_data(parser, (const gchar *)payload_data, strlen(payload_data), NULL);
	root_node = json_parser_get_root(parser);
	root = json_node_get_object(root_node);

	queues = json_object_get_array_member(root, "queues");
	json_array_foreach_element(queues, _podmq_client_get_all_queue_names_foreach_cb, (gpointer)ret);

	return ret;
}




static
void 
_podmq_client_get_manager_summary_foreach_cb(
	JsonArray			*array,
	guint				index,
	JsonNode			*element_node,
	gpointer			user_data
){
	GArray *data;
	JsonObject *object;
	PodMQQueueSummary summary;

	data = user_data;
	memset(&summary, 0x00, sizeof(summary));
	object = json_node_get_object(element_node);

	summary.queue_name = g_strdup(json_object_get_string_member(object, "queue_name"));
	summary.item_count = (gint)json_object_get_int_member(object, "count");

	g_array_append_val(data, summary);
}




PODMQ_API 
GArray *
podmq_client_get_manager_summary(
	PodMQClient			*self
){
	g_return_val_if_fail(PODMQ_IS_CLIENT(self), NULL);

	GArray *ret;
	gchar *resp_data;
	g_autoptr(GBytes) resp;
	g_autoptr(JsonParser) parser;
	JsonNode *root_node;
	JsonObject *root;
	JsonArray *queues;

	resp = NULL;
	ret = NULL;
	parser = NULL;

	resp = _podmq_client_api_caller(
		self->url_get_manager_summary,
		NULL,
		SOUP_METHOD_GET
	);

	g_return_val_if_fail(resp, NULL);

	ret = g_array_new(TRUE, TRUE, sizeof(PodMQQueueSummary));
	resp_data = (gchar *)g_bytes_get_data(resp, NULL);

	parser = json_parser_new();
	json_parser_load_from_data(parser, (const gchar *)resp_data, strlen((const gchar *)resp_data), NULL);
	root_node = json_parser_get_root(parser);
	root = json_node_get_object(root_node);

	queues = json_object_get_array_member(root, "queues");
	json_array_foreach_element(queues, _podmq_client_get_manager_summary_foreach_cb, (gpointer)ret);

	return ret;

}




PODMQ_API 
void 
podmq_client_free_manager_summary_result(
	GArray				*result
){
	g_return_if_fail(result);

	guint i;

	for (
		i = 0;
		i < result->len;
		i++
	){
		PodMQQueueSummary summary;

		summary = g_array_index(result, PodMQQueueSummary, i);
		g_free(summary.queue_name);
	}

	g_array_unref(result);
}




static
void 
_podmq_client_peek_foreach_cb(
	JsonArray			*array,
	guint				index,
	JsonNode			*element_node,
	gpointer			user_data
){
	GArray *data;
	g_autoptr(JsonGenerator) generator;
	g_autofree gchar *raw_json;
	PodMQMessage *msg;

	data = user_data;
	raw_json = NULL;
	generator = json_generator_new();
	
	json_generator_set_root(generator, element_node);
	raw_json = json_generator_to_data(generator, NULL);

	msg = podmq_message_new_from_json(raw_json);
	
	if (NULL != msg)
	{ g_array_append_val(data, msg); }
}




PODMQ_API 
GArray *
podmq_client_peek(
	PodMQClient			*self
){
	g_return_val_if_fail(PODMQ_IS_CLIENT(self), NULL);

	GArray *ret;
	gchar *resp_data;
	g_autoptr(GBytes) resp;
	g_autoptr(JsonParser) parser;
	JsonNode *root_node;
	JsonArray *messages;

	resp = NULL;
	ret = NULL;
	parser = NULL;

	resp = _podmq_client_api_caller(
		self->url_peek,
		NULL,
		SOUP_METHOD_GET
	);

	g_return_val_if_fail(resp, NULL);

	resp_data = g_bytes_get_data(resp, NULL);
	g_return_val_if_fail(resp_data, NULL);

	ret = g_array_new(TRUE, TRUE, sizeof(PodMQMessage *));
	parser = json_parser_new();

	json_parser_load_from_data(parser, (const gchar *)resp_data, strlen((const gchar *)resp_data), NULL);

	root_node = json_parser_get_root(parser);
	messages = json_node_get_array(root_node);

	json_array_foreach_element(messages, _podmq_client_peek_foreach_cb, ret);

	return ret;
}
