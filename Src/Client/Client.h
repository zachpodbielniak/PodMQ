/*


 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef PODMQ_CLIENT_H
#define PODMQ_CLIENT_H



#include "../PodMQ.h"
#include "../Message/Message.h"



#define PODMQ_TYPE_CLIENT			(podmq_client_get_type())
G_DECLARE_FINAL_TYPE(PodMQClient, podmq_client, PODMQ, CLIENT, GObject);




typedef struct
{
	gchar				*queue_name;
	gint 				item_count;
} PodMQQueueSummary;




PODMQ_API
PodMQClient *
podmq_client_new(
	const gchar			*host,
	guint16				port,
	const gchar			*manager,
	const gchar			*queue,
	const gchar			*api_token,
	gboolean 			http_secure
);




PODMQ_API 
PodMQMessage *
podmq_client_get(
	PodMQClient			*self
);




PODMQ_API 
gboolean
podmq_client_put(
	PodMQClient			*self,
	PodMQMessage			*msg
);




PODMQ_API 
gboolean
podmq_client_put_many(
	PodMQClient			*self,
	GArray				*messages
);




PODMQ_API 
gboolean
podmq_client_create(
	PodMQClient			*self,
	const gchar 			*name
);




PODMQ_API 
GArray *
podmq_client_get_all_queue_names(
	PodMQClient			*self
);




PODMQ_API 
GArray *
podmq_client_get_manager_summary(
	PodMQClient			*self
);




PODMQ_API 
void 
podmq_client_free_manager_summary_result(
	GArray				*result
);




PODMQ_API 
GArray *
podmq_client_peek(
	PodMQClient			*self
);




#endif