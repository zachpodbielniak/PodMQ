/*


 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "Queue.h"




static void (*ancestor_dispose)(GObject *) = NULL;
static void (*ancestor_finalize)(GObject *) = NULL;




struct _PodMQQueue
{
	GObject				parent;
	gchar				*queue_name;
	gint64				number_of_items;
	GQueue				*queue_data;
	GMutex				lock;
};




G_DEFINE_TYPE(PodMQQueue, podmq_queue, G_TYPE_OBJECT);




static 
void 
podmq_queue_dispose(
	PodMQQueue			*self
){
	g_return_if_fail(PODMQ_IS_QUEUE(self));

	self = PODMQ_QUEUE(self);
	ancestor_dispose(G_OBJECT(self));
}




static 
void 
podmq_queue_finalize(
	PodMQQueue			*self
){
	g_return_if_fail(PODMQ_IS_QUEUE(self));

	self = PODMQ_QUEUE(self);
	ancestor_finalize(G_OBJECT(self));
}




static 
void 
podmq_queue_init(
	PodMQQueue			*self
){ (void)0; }




static 
void 
podmq_queue_class_init(
	PodMQQueueClass			*klazz
){
	GObjectClass *ancestor_klazz;

	ancestor_klazz = G_OBJECT_CLASS(klazz);

	ancestor_dispose = ancestor_klazz->dispose;
	ancestor_finalize = ancestor_klazz->finalize;

	ancestor_klazz->dispose = (void (*)(GObject *))podmq_queue_dispose;
	ancestor_klazz->finalize = (void (*)(GObject *))podmq_queue_finalize;
}




PODMQ_API 
PodMQQueue *
podmq_queue_new(
	const gchar 			*queue_name
){
	g_return_val_if_fail(queue_name, NULL);

	PodMQQueue *self;

	self = g_object_new(PODMQ_TYPE_QUEUE, NULL);

	g_mutex_init(&(self->lock));
	self->number_of_items = 0;
	self->queue_name = g_strdup(queue_name);
	self->queue_data = g_queue_new();

	return PODMQ_QUEUE(self);
}




PODMQ_API 
PodMQQueue *
podmq_queue_new_from_copy(
	PodMQQueue			*to_be_copied
){
	g_return_val_if_fail(PODMQ_IS_QUEUE(to_be_copied), NULL);

	PodMQQueue *self;

	self = g_object_new(PODMQ_TYPE_QUEUE, NULL);

	g_mutex_lock(&(to_be_copied->lock));

	g_mutex_init(&(self->lock));
	self->number_of_items = to_be_copied->number_of_items;
	self->queue_name = g_strdup(to_be_copied->queue_name);
	self->queue_data = g_queue_copy(to_be_copied->queue_data);

	g_mutex_unlock(&(to_be_copied->lock));
	return self;
}




PODMQ_API 
const gchar *
podmq_queue_get_name(
	PodMQQueue			*self
){
	g_return_val_if_fail(PODMQ_IS_QUEUE(self), NULL);
	return self->queue_name;
}




PODMQ_API 
gint64 
podmq_queue_get_queue_item_count(
	PodMQQueue			*self
){
	g_return_val_if_fail(PODMQ_IS_QUEUE(self), -1);
	gint64 ret;

	g_mutex_lock(&(self->lock));
	ret = (gint64)g_queue_get_length(self->queue_data);
	g_mutex_unlock(&(self->lock));

	return ret;
}




PODMQ_API 
PodMQMessage *
podmq_queue_get(
	PodMQQueue			*self
){
	g_return_val_if_fail(PODMQ_IS_QUEUE(self), NULL);
	PodMQMessage *ret;

	g_mutex_lock(&(self->lock));
	ret = (PodMQMessage *)g_queue_pop_head(self->queue_data);
	g_mutex_unlock(&(self->lock));

	return ret;
}




PODMQ_API 
gboolean 
podmq_queue_put(
	PodMQQueue			*self,
	PodMQMessage			*msg
){
	g_return_val_if_fail(PODMQ_IS_QUEUE(self), FALSE);

	g_mutex_lock(&(self->lock));
	g_queue_push_tail(self->queue_data, msg);
	g_mutex_unlock(&(self->lock));

	return TRUE;
}




PODMQ_API 
GQueue *
podmq_queue_peek(
	PodMQQueue			*self
){
	g_return_val_if_fail(PODMQ_IS_QUEUE(self), NULL);

	GQueue *ret;
	PodMQMessage *current;
	PodMQMessage *copy;
	gint i;

	ret = g_queue_new();

	g_mutex_lock(&(self->lock));

	for (
		i = 0;
		i < (gint)g_queue_get_length(self->queue_data);
		i++
	){
		current = (PodMQMessage *)g_queue_peek_nth(
			self->queue_data,
			(guint)i
		);

		if (NULL != current)
		{
			copy = podmq_message_copy(current);
			g_queue_push_tail(ret, copy);
		}
	}

	g_mutex_unlock(&(self->lock));

	return ret;
}
