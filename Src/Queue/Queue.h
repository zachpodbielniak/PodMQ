/*


 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef PODMQ_QUEUE_H
#define PODMQ_QUEUE_H


#include "../PodMQ.h"
#include "../Message/Message.h"




#define PODMQ_TYPE_QUEUE			(podmq_queue_get_type())
G_DECLARE_FINAL_TYPE(PodMQQueue, podmq_queue, PODMQ, QUEUE, GObject);




PODMQ_API 
PodMQQueue *
podmq_queue_new(
	const gchar 			*queue_name
);




PODMQ_API 
PodMQQueue *
podmq_queue_new_from_copy(
	PodMQQueue			*to_be_copied
);




PODMQ_API 
const gchar *
podmq_queue_get_name(
	PodMQQueue			*self
);




PODMQ_API 
gint64 
podmq_queue_get_queue_item_count(
	PodMQQueue			*self
);




PODMQ_API 
PodMQMessage *
podmq_queue_get(
	PodMQQueue			*self
);




PODMQ_API 
gboolean 
podmq_queue_put(
	PodMQQueue			*self,
	PodMQMessage			*msg
);




PODMQ_API 
GQueue *
podmq_queue_peek(
	PodMQQueue			*self
);






#endif