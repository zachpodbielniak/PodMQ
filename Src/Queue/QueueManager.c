/*


 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "QueueManager.h"




static void (*ancestor_dispose)(GObject *) = NULL;
static void (*ancestor_finalize)(GObject *) = NULL;




struct _PodMQQueueManager
{
	GObject				parent;
	gchar				*name;
	GHashTable			*queues;
	GList				*queue_names;
	guint64				number_of_queues;
	GMutex 				lock;
};



G_DEFINE_TYPE(PodMQQueueManager, podmq_queue_manager, G_TYPE_OBJECT);




void 
_podmq_queue_manager_create_array_of_queues(
	gpointer			key,
	gpointer			value,
	gpointer			user_data
){
	PodMQQueue *queue;
	GArray *output;

	queue = value;
	output = user_data;

	g_array_append_val(output, queue);
}




static
void 
_podmq_queue_manager_create_array_of_names(
	gpointer			key,
	gpointer			value,
	gpointer			user_data
){
	gchar *queue_name;
	GArray *output;

	queue_name = key;
	output = user_data;

	g_array_append_val(output, queue_name);
}




static 
void 
podmq_queue_manager_dispose(
	PodMQQueueManager		*self
){
	g_return_if_fail(PODMQ_IS_QUEUE_MANAGER(self));

	self = PODMQ_QUEUE_MANAGER(self);
	ancestor_dispose(G_OBJECT(self));
}




static 
void 
podmq_queue_manager_finalize(
	PodMQQueueManager		*self
){
	g_return_if_fail(PODMQ_IS_QUEUE_MANAGER(self));

	self = PODMQ_QUEUE_MANAGER(self);
	ancestor_finalize(G_OBJECT(self));
}




static 
void 
podmq_queue_manager_init(
	PodMQQueueManager		*self
){ (void)0; }




static 
void 
podmq_queue_manager_class_init(
	PodMQQueueManagerClass		*klazz
){
	GObjectClass *ancestor_klazz;
	
	ancestor_klazz = G_OBJECT_CLASS(klazz);

	ancestor_dispose = ancestor_klazz->dispose;
	ancestor_finalize = ancestor_klazz->finalize;

	ancestor_klazz->dispose = (void (*)(GObject *))podmq_queue_manager_dispose;
	ancestor_klazz->finalize = (void (*)(GObject *))podmq_queue_manager_finalize;
}




PODMQ_API
PodMQQueueManager *
podmq_queue_manager_new(
	const gchar 			*name
){
	PodMQQueueManager *self;

	self = g_object_new(PODMQ_TYPE_QUEUE_MANAGER, NULL);

	g_mutex_init(&(self->lock));
	self->name = g_strdup(name);
	self->number_of_queues = 0;
	self->queue_names = g_list_alloc();
	self->queues = g_hash_table_new(
		g_str_hash, 
		g_str_equal
	);
	
	return self;
}




PODMQ_API
const gchar *
podmq_queue_manager_get_name(
	PodMQQueueManager 		*self
){
	g_return_val_if_fail(PODMQ_IS_QUEUE_MANAGER(self), NULL);
	return self->name;
}




PODMQ_API 
PodMQQueue *
podmq_queue_manager_get_queue_by_name(
	PodMQQueueManager		*self,
	const gchar			*name
){
	g_return_val_if_fail(PODMQ_IS_QUEUE_MANAGER(self), NULL);

	PodMQQueue *ret;

	g_mutex_lock(&(self->lock));
	ret = g_hash_table_lookup(self->queues, (gconstpointer)name);
	g_mutex_unlock(&(self->lock));

	return ret;
}




PODMQ_API 
GHashTable *
podmq_queue_manager_get_queues(
	PodMQQueueManager		*self
){
	g_return_val_if_fail(PODMQ_IS_QUEUE_MANAGER(self), NULL);
	return self->queues;
}




PODMQ_API 
GArray *
podmq_queue_manager_get_queues_as_array(
	PodMQQueueManager		*self
){
	g_return_val_if_fail(PODMQ_IS_QUEUE_MANAGER(self), NULL);

	GArray *ret;
	ret = g_array_new(TRUE, TRUE, sizeof(gchar *));

	g_mutex_lock(&(self->lock));
	g_hash_table_foreach(
		self->queues, 
		_podmq_queue_manager_create_array_of_queues, 
		ret
	);
	g_mutex_unlock(&(self->lock));

	return ret;
}




PODMQ_API 
GArray *
podmq_queue_manager_get_queue_names(
	PodMQQueueManager		*self
){
	g_return_val_if_fail(PODMQ_IS_QUEUE_MANAGER(self), NULL);

	GArray *ret;
	ret = g_array_new(TRUE, TRUE, sizeof(gchar *));

	g_mutex_lock(&(self->lock));
	g_hash_table_foreach(
		self->queues, 
		_podmq_queue_manager_create_array_of_names, 
		ret
	);
	g_mutex_unlock(&(self->lock));

	return ret;
}




PODMQ_API 
guint64 
podmq_queue_manager_get_number_of_queues(
	PodMQQueueManager		*self
){
	g_return_val_if_fail(PODMQ_IS_QUEUE_MANAGER(self), (guint64)-1);
	return self->number_of_queues;
}




PODMQ_API 
gboolean 
podmq_queue_manager_add(
	PodMQQueueManager		*self,
	PodMQQueue			*queue
){
	g_return_val_if_fail(PODMQ_IS_QUEUE_MANAGER(self), FALSE);

	g_mutex_lock(&(self->lock));
	g_hash_table_insert(
		self->queues,
		(gpointer)podmq_queue_get_name(queue),
		queue
	);
	g_mutex_unlock(&(self->lock));

	return TRUE;
}




PODMQ_API 
gboolean 
podmq_queue_manager_remove(
	PodMQQueueManager		*self,
	PodMQQueue			*queue
){
	g_return_val_if_fail(PODMQ_IS_QUEUE_MANAGER(self), FALSE);

	return podmq_queue_manager_remove_by_name(
		self,
		podmq_queue_get_name(queue)
	);
}




PODMQ_API 
gboolean 
podmq_queue_manager_remove_by_name(
	PodMQQueueManager		*self,
	const gchar			*name
){
	g_return_val_if_fail(PODMQ_IS_QUEUE_MANAGER(self), FALSE);

	gboolean ret;

	g_mutex_lock(&(self->lock));
	ret = g_hash_table_remove(
		self->queues,
		(gconstpointer)name
	);
	g_mutex_unlock(&(self->lock));

	return ret;
}