/*


 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef PODMQ_QUEUE_MANAGER_H
#define PODMQ_QUEUE_MANAGER_H


#include "../PodMQ.h"
#include "Queue.h"




#define PODMQ_TYPE_QUEUE_MANAGER			(podmq_queue_manager_get_type())
G_DECLARE_FINAL_TYPE(PodMQQueueManager, podmq_queue_manager, PODMQ, QUEUE_MANAGER, GObject);








PODMQ_API
PodMQQueueManager *
podmq_queue_manager_new(
	const gchar			*name
);




PODMQ_API
const gchar *
podmq_queue_manager_get_name(
	PodMQQueueManager		*self
);




PODMQ_API 
PodMQQueue *
podmq_queue_manager_get_queue_by_name(
	PodMQQueueManager		*self,
	const gchar			*name
);




PODMQ_API 
GHashTable *
podmq_queue_manager_get_queues(
	PodMQQueueManager		*self
);




PODMQ_API 
GArray *
podmq_queue_manager_get_queues_as_array(
	PodMQQueueManager		*self
);




PODMQ_API 
GArray *
podmq_queue_manager_get_queue_names(
	PodMQQueueManager		*self
);




PODMQ_API 
guint64 
podmq_queue_manager_get_number_of_queues(
	PodMQQueueManager		*self
);




PODMQ_API 
gboolean 
podmq_queue_manager_add(
	PodMQQueueManager		*self,
	PodMQQueue			*queue
);




PODMQ_API 
gboolean 
podmq_queue_manager_remove(
	PodMQQueueManager		*self,
	PodMQQueue			*queue
);




PODMQ_API 
gboolean 
podmq_queue_manager_remove_by_name(
	PodMQQueueManager		*self,
	const gchar			*name
);



#endif
