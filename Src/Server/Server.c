/*


 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "Server.h"
#include "ApiV1.c"




static void (*ancestor_dispose)(GObject *) = NULL;
static void (*ancestor_finalize)(GObject *) = NULL;




struct _PodMQServer
{
	GObject				parent;
	SoupServer			*server;
	gchar				*bind_address;
	guint16				bind_port;
	PodMQQueueManager		*default_manager;
	GHashTable			*queue_managers;
	GMutex 				lock;
	GMainContext			*main_context;
	GMainLoop			*main_loop;
	GArray				*api_tokens;
};




G_DEFINE_TYPE(PodMQServer, podmq_server, G_TYPE_OBJECT);




static 
void 
_podmq_server_soup_init(
	PodMQServer			*self
){
	self->server = soup_server_new(NULL, NULL);
	g_autoptr(GSocketAddress) addr;

	addr = G_SOCKET_ADDRESS(
		g_inet_socket_address_new_from_string(
			(const gchar *)self->bind_address,
			(guint)self->bind_port
		)
	),

	soup_server_listen(
		self->server,
		addr,
		0,
		NULL
	);
	
	self->main_context = g_main_context_get_thread_default();
	self->main_loop = g_main_loop_new(self->main_context, FALSE);
}




static 
void 
_podmq_server_root_handler(
	SoupServer			*server,
	SoupServerMessage		*msg,
	const gchar			*path,
	GHashTable			*query,
	gpointer			user_data
){
	gchar *ret;
	ret = "PodMQ v2.0\r\n";

	soup_server_message_set_status(msg, SOUP_STATUS_OK, NULL);
	soup_server_message_set_response(
		msg,
		"application/json",
		SOUP_MEMORY_COPY,
		(const gchar *)ret,
		strlen(ret)
	);
}




static 
void 
_podmq_server_register_handlers(
	PodMQServer			*self
){
	soup_server_add_handler(self->server, "/", _podmq_server_root_handler, self, NULL);
	podmq_apiv1_register_handlers(self, self->server);
}



static 
void 
podmq_server_dispose(
	PodMQServer			*self
){
	g_return_if_fail(PODMQ_IS_SERVER(self));

	self = PODMQ_SERVER(self);
	ancestor_dispose(G_OBJECT(self));
}




static 
void 
podmq_server_finalize(
	PodMQServer			*self
){
	g_return_if_fail(PODMQ_IS_SERVER(self));

	self = PODMQ_SERVER(self);
	ancestor_finalize(G_OBJECT(self));
}




static 
void 
podmq_server_init(
	PodMQServer			*self
){ (void)0; }




static 
void 
podmq_server_class_init(
	PodMQServerClass		*klazz
){
	GObjectClass *ancestor_klazz;
	
	ancestor_klazz = G_OBJECT_CLASS(klazz);

	ancestor_dispose = ancestor_klazz->dispose;
	ancestor_finalize = ancestor_klazz->finalize;

	ancestor_klazz->dispose = (void (*)(GObject *))podmq_server_dispose;
	ancestor_klazz->finalize = (void (*)(GObject *))podmq_server_finalize;
}




PODMQ_API 
PodMQServer *
podmq_server_new(
	const gchar			*address,
	guint16				port,
	const GArray			*api_tokens
){
	g_return_val_if_fail(address, NULL);

	PodMQServer *self;

	if (0 == port)
	{ port = 9001; }

	self = g_object_new(PODMQ_TYPE_SERVER, NULL);

	g_mutex_init(&(self->lock));
	self->bind_address = g_strdup(address);
	self->bind_port = port;
	self->queue_managers = g_hash_table_new(
		g_str_hash,
		g_str_equal
	);
	self->api_tokens = (GArray *)api_tokens;
	
	_podmq_server_soup_init(self);
	_podmq_server_register_handlers(self);

	return self;	
}




PODMQ_API 
gboolean 
podmq_server_register_manager(
	PodMQServer			*self,
	PodMQQueueManager		*manager
){
	g_return_val_if_fail(PODMQ_IS_SERVER(self), FALSE);
	g_return_val_if_fail(PODMQ_IS_QUEUE_MANAGER(manager), manager);

	g_mutex_lock(&(self->lock));
	g_hash_table_insert(
		self->queue_managers,
		(gpointer)podmq_queue_manager_get_name(manager),
		(gpointer)manager
	);
	g_mutex_unlock(&(self->lock));

	return TRUE;
}




PODMQ_API 
gboolean 
podmq_server_set_default_manager(
	PodMQServer			*self,
	PodMQQueueManager		*manager
){
	g_return_val_if_fail(PODMQ_IS_SERVER(self), FALSE);
	g_return_val_if_fail(PODMQ_IS_QUEUE_MANAGER(manager), manager);


	g_mutex_lock(&(self->lock));
	self->default_manager = manager;
	g_hash_table_insert(
		self->queue_managers,
		(gpointer)podmq_queue_manager_get_name(manager),
		(gpointer)manager
	);
	g_mutex_unlock(&(self->lock));

	return TRUE;
}




PODMQ_API 
PodMQQueueManager *
podmq_server_get_default_manager(
	PodMQServer			*self
){
	g_return_val_if_fail(PODMQ_IS_SERVER(self), NULL);
	return self->default_manager;
}




PODMQ_API 
PodMQQueueManager *
podmq_server_get_manager_by_name(
	PodMQServer			*self,
	const gchar			*name
){
	g_return_val_if_fail(PODMQ_IS_SERVER(self), NULL);
	g_return_val_if_fail(name, NULL);

	PodMQQueueManager *ret;

	g_mutex_lock(&(self->lock));
	ret = g_hash_table_lookup(
		self->queue_managers,
		name
	);
	g_mutex_unlock(&(self->lock));

	return ret;
}




PODMQ_API 
void 
podmq_server_serve(
	PodMQServer			*self
){
	g_return_if_fail(PODMQ_IS_SERVER(self));
	g_main_loop_run(self->main_loop);
}




PODMQ_API 
gboolean podmq_server_token_is_valid(
	PodMQServer			*self,
	const gchar			*api_token
){
	g_return_val_if_fail(PODMQ_IS_SERVER(self), FALSE);
	
	gint i;
	const gchar *iterable;
	gboolean ret;

	ret = FALSE;

	if (NULL == self->api_tokens)
	{ return TRUE; }

	for (
		i = 0;
		i < (gint)self->api_tokens->len;
		i++
	){
		iterable = g_array_index(self->api_tokens, const gchar *, i);

		if (0 == g_strcmp0(iterable, api_token))
		{ return TRUE; }
	}

	return ret;
}