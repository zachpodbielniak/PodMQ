/*


 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef PODMQ_SERVER_H
#define PODMQ_SERVER_H


#include "../PodMQ.h"
#include "../Message/Message.h"
#include "../Queue/QueueManager.h"



#define PODMQ_TYPE_SERVER			(podmq_server_get_type())
G_DECLARE_FINAL_TYPE(PodMQServer, podmq_server, PODMQ, SERVER, GObject);




PODMQ_API 
PodMQServer *
podmq_server_new(
	const gchar			*address,
	guint16				port,
	const GArray			*api_tokens
);




PODMQ_API 
gboolean 
podmq_server_register_manager(
	PodMQServer			*self,
	PodMQQueueManager		*manager
);




PODMQ_API 
gboolean 
podmq_server_set_default_manager(
	PodMQServer			*self,
	PodMQQueueManager		*manager
);




PODMQ_API 
PodMQQueueManager *
podmq_server_get_default_manager(
	PodMQServer			*self
);




PODMQ_API 
PodMQQueueManager *
podmq_server_get_manager_by_name(
	PodMQServer			*self,
	const gchar			*name
);




PODMQ_API 
void 
podmq_server_serve(
	PodMQServer			*self
);




PODMQ_API 
gboolean podmq_server_token_is_valid(
	PodMQServer			*self,
	const gchar			*api_token
);



#endif