/*


 ____           _ __  __  ___  
|  _ \ ___   __| |  \/  |/ _ \
| |_) / _ \ / _` | |\/| | | | |
|  __/ (_) | (_| | |  | | |_| |
|_|   \___/ \__,_|_|  |_|\__\_\

Network Message Queue In C.
Copyright (C) 2019-2022 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef PODMQ_APIV1_C
#define PODMQ_APIV1_C


#include "Server.h"



static const gchar *err_apiv1_400 = "{\"error\":400}";
static const gchar *err_apiv1_401 = "{\"error\":401}";
static const gchar *err_apiv1_500 = "{\"error\":500}";



#define ERR_400()								\
	soup_server_message_set_status(msg, SOUP_STATUS_BAD_REQUEST, NULL);	\
	soup_server_message_set_response(					\
		msg,								\
		"application/json",						\
		SOUP_MEMORY_STATIC,						\
		err_apiv1_400,							\
		strlen(err_apiv1_400)						\
	);




#define ERR_401()								\
	soup_server_message_set_status(msg, SOUP_STATUS_UNAUTHORIZED, NULL);	\
	soup_server_message_set_response(					\
		msg,								\
		"application/json",						\
		SOUP_MEMORY_STATIC,						\
		err_apiv1_401,							\
		strlen(err_apiv1_401)						\
	);




#define ERR_500()									\
	soup_server_message_set_status(msg, SOUP_STATUS_INTERNAL_SERVER_ERROR, NULL);	\
	soup_server_message_set_response(						\
		msg,									\
		"application/json",							\
		SOUP_MEMORY_STATIC,							\
		err_apiv1_500,								\
		strlen(err_apiv1_500)							\
	);



#define OK_204()								\
	soup_server_message_set_status(msg, SOUP_STATUS_BAD_REQUEST, NULL);	\
	soup_server_message_set_response(					\
		msg,								\
		NULL,								\
		SOUP_MEMORY_COPY,						\
		NULL,								\
		0								\
	);




static
gboolean
_podmq_apiv1_query_helper(
	SoupServer			*server,
	SoupServerMessage		*msg,
	PodMQServer			*mq_server,
	GHashTable			*query,
	PodMQQueue			**queue,
	PodMQQueueManager		**manager, 
	PodMQMessage			**message,
	gchar 				**queue_name,
	gchar 				**manager_name,
	gboolean 			*pretty,
	gboolean 			*token_valid
){
	if (NULL == token_valid)
	{ 
		ERR_500();
		return FALSE; 
	}
	else 
	{ 
		*token_valid = podmq_server_token_is_valid(
			mq_server,
			g_hash_table_lookup(query, "api-token")
		);

		if (FALSE == *token_valid)
		{
			ERR_401();
			return FALSE;
		}
	}

	if (NULL != pretty)
	{ *pretty = FALSE; }

	if (NULL != queue_name)
	{ *queue_name = g_hash_table_lookup(query, "queue"); }

	if (NULL != manager_name)
	{ *manager_name = g_hash_table_lookup(query, "manager");} 

	if (0 == g_strcmp0(g_hash_table_lookup(query, "pretty"), "true"))
	{ *pretty = TRUE; }


	if (NULL == *manager_name)
	{
		*manager = podmq_server_get_default_manager(mq_server);
		*manager_name = (gchar *)podmq_queue_manager_get_name(*manager);
	}
	else 
	{ *manager = podmq_server_get_manager_by_name(mq_server, *manager_name); }

	if (NULL == *queue_name)
	{ 
		ERR_400(); 
		return FALSE;
	}
	else 
	{ *queue = podmq_queue_manager_get_queue_by_name(*manager, *queue_name); }

	if (NULL == *queue)
	{ 
		ERR_400(); 
		return FALSE;
	}

	if (NULL != message)
	{ 
		*message = podmq_queue_get(*queue); 
		
		if (NULL == *message)
		{ 
			OK_204(); 
			return FALSE;
		}
	}
	
	return TRUE;
}




static
gboolean
_podmq_apiv1_simple_helper(
	SoupServer			*server,
	SoupServerMessage		*msg,
	PodMQServer			*mq_server,
	GHashTable			*query,
	PodMQQueue			**queue,
	PodMQQueueManager		**manager, 
	gchar 				**queue_name,
	gchar 				**manager_name,
	gboolean 			*pretty
){
	gboolean token_valid;
	
	token_valid = podmq_server_token_is_valid(
		mq_server,
		g_hash_table_lookup(query, "api-token")
	);

	if (FALSE == token_valid)
	{ return FALSE; }


	if (NULL != pretty)
	{ *pretty = FALSE; }
	
	if (NULL != queue_name)
	{ *queue_name = g_hash_table_lookup(query, "queue"); }

	if (NULL != manager_name)
	{ *manager_name = g_hash_table_lookup(query, "manager"); }

	if (0 == g_strcmp0(g_hash_table_lookup(query, "pretty"), "true"))
	{ *pretty = TRUE; }


	if (NULL == *manager_name)
	{
		*manager = podmq_server_get_default_manager(mq_server);
		*manager_name = (gchar *)podmq_queue_manager_get_name(*manager);
	}
	else 
	{ *manager = podmq_server_get_manager_by_name(mq_server, *manager_name); }

	if (NULL != queue_name)
	{
		if (NULL != *queue_name) 
		{ *queue = podmq_queue_manager_get_queue_by_name(*manager, *queue_name); }
	}


	return TRUE;
}



static 
void 
_podmq_apiv1_create(
	SoupServer			*server,
	SoupServerMessage		*msg,
	const gchar			*path,
	GHashTable			*query,
	gpointer			user_data
){
	PodMQServer *mq_server;
	PodMQQueue *queue;
	PodMQQueueManager *manager;
	gchar *queue_name;
	gchar *manager_name;
	gboolean helper_ret;
	gboolean api_valid;

	mq_server = user_data;
	helper_ret = _podmq_apiv1_query_helper(
		server,
		msg,
		mq_server,
		query,
		&queue,
		&manager,
		NULL,
		&queue_name,
		&manager_name,
		NULL,
		&api_valid
	);

	if (
		NULL != manager &&
		NULL != queue_name
	){
		queue = podmq_queue_new(queue_name);
		podmq_queue_manager_add(manager, queue);
	}

	soup_server_message_set_status(msg, SOUP_STATUS_ACCEPTED, NULL);
	soup_server_message_set_response(
		msg,
		NULL,
		SOUP_MEMORY_STATIC,
		NULL,
		0
	);
}



static 
void 
_podmq_apiv1_get(
	SoupServer			*server,
	SoupServerMessage		*msg,
	const gchar			*path,
	GHashTable			*query,
	gpointer			user_data
){
	PodMQServer *mq_server;
	PodMQQueue *queue;
	PodMQQueueManager *manager;
	g_autoptr(PodMQMessage) message;
	gchar *ret;
	gchar *queue_name;
	gchar *manager_name;
	gboolean pretty;
	gboolean helper_ret;
	gboolean api_valid;

	mq_server = user_data;
	message = NULL;

	helper_ret = _podmq_apiv1_query_helper(
		server,
		msg,
		mq_server,
		query,
		&queue,
		&manager,
		&message,
		&queue_name,
		&manager_name,
		&pretty,
		&api_valid
	);

	if (FALSE == helper_ret)
	{ return; }


	if (TRUE == pretty)
	{ ret = (gchar *)podmq_message_get_data_pretty(message); }
	else 
	{ ret = (gchar *)podmq_message_get_data_raw(message); }

	soup_server_message_set_status(msg, SOUP_STATUS_OK, NULL);
	soup_server_message_set_response(
		msg,
		"application/json",
		SOUP_MEMORY_COPY,
		(const gchar *)ret,
		strlen(ret)
	);
}




static 
void 
_podmq_apiv1_put(
	SoupServer			*server,
	SoupServerMessage		*msg,
	const gchar			*path,
	GHashTable			*query,
	gpointer			user_data
){
	PodMQServer *mq_server;
	PodMQQueue *queue;
	PodMQQueueManager *manager;
	SoupMessageBody *body;
	PodMQMessage *message;
	gchar *ret;
	gchar *queue_name;
	gchar *manager_name;
	gboolean pretty;
	gboolean helper_ret;
	gboolean api_valid;

	mq_server = user_data;

	helper_ret = _podmq_apiv1_query_helper(
		server,
		msg,
		mq_server,
		query,
		&queue,
		&manager,
		NULL,
		&queue_name,
		&manager_name,
		&pretty,
		&api_valid
	);

	if (FALSE == helper_ret)
	{ return; }
	
	if (FALSE == api_valid)
	{
		ERR_401();
		return;
	}
	

	body = soup_server_message_get_request_body(msg);

	message = podmq_message_new_from_json(
		body->data
	);

	if (TRUE == pretty)
	{ ret = (gchar *)podmq_message_get_data_pretty(message); }
	else 
	{ ret = (gchar *)podmq_message_get_data_raw(message); }

	podmq_queue_put(queue, message);
	
	soup_server_message_set_status(msg, SOUP_STATUS_OK, NULL);
	soup_server_message_set_response(
		msg,
		"application/json",
		SOUP_MEMORY_COPY,
		(const gchar *)ret,
		strlen(ret)
	);
}




static
void 
_podmq_apiv1_put_many_array_cb(
	JsonArray			*array,
	guint				index,
	JsonNode			*element_node,
	gpointer			user_data
){
	g_autoptr(JsonGenerator) generator;
	g_autofree gchar *raw_json;
	PodMQQueue *queue;
	PodMQMessage *msg;

	queue = user_data;
	raw_json = NULL;
	generator = json_generator_new();
	
	json_generator_set_root(generator, element_node);
	raw_json = json_generator_to_data(generator, NULL);

	msg = podmq_message_new_from_json(raw_json);
	
	podmq_queue_put(queue, msg);
}




static 
void 
_podmq_apiv1_put_many(
	SoupServer			*server,
	SoupServerMessage		*msg,
	const gchar			*path,
	GHashTable			*query,
	gpointer			user_data
){
	PodMQServer *mq_server;
	PodMQQueue *queue;
	PodMQQueueManager *manager;
	SoupMessageBody *body;
	PodMQMessage *message;
	gchar *ret;
	gchar *queue_name;
	gchar *manager_name;
	gboolean pretty;
	gboolean helper_ret;
	gboolean api_valid;
	g_autoptr(JsonParser) parser;
	JsonNode *root;
	JsonArray *array;


	mq_server = user_data;
	parser = NULL;
	root = NULL;
	array = NULL;

	helper_ret = _podmq_apiv1_query_helper(
		server,
		msg,
		mq_server,
		query,
		&queue,
		&manager,
		NULL,
		&queue_name,
		&manager_name,
		&pretty,
		&api_valid
	);

	if (FALSE == helper_ret)
	{ return; }
	
	if (FALSE == api_valid)
	{
		ERR_401();
		return;
	}
	

	body = soup_server_message_get_request_body(msg);

	parser = json_parser_new();
	json_parser_load_from_data(parser, body->data, strlen(body->data), NULL);
	root = json_parser_get_root(parser);
	array = json_node_get_array(root);

	json_array_foreach_element(array, _podmq_apiv1_put_many_array_cb, (gpointer)queue);

	soup_server_message_set_status(msg, SOUP_STATUS_NO_CONTENT, NULL);
	soup_server_message_set_response(
		msg,
		"application/json",
		SOUP_MEMORY_COPY,
		NULL,
		0
	);
}




static 
void 
_podmq_apiv1_get_all_queue_names(
	SoupServer			*server,
	SoupServerMessage		*msg,
	const gchar			*path,
	GHashTable			*query,
	gpointer			user_data
){
	PodMQServer *mq_server;
	PodMQQueue *queue;
	PodMQQueueManager *manager;
	gchar *ret;
	gchar *queue_name;
	gchar *manager_name;
	gboolean pretty;
	gboolean helper_ret;
	g_autoptr(JsonBuilder) builder;
	g_autoptr(JsonNode) root;
	g_autoptr(JsonGenerator) generator;
	g_autoptr(GArray) all_queues;
	gint i;

	mq_server = user_data;
	builder = NULL;
	root = NULL;
	generator = NULL;
	all_queues = NULL;

	helper_ret = _podmq_apiv1_simple_helper(
		server,
		msg,
		mq_server,
		query,
		&queue,
		&manager,
		&queue_name,
		&manager_name,
		&pretty
	);

	if (FALSE == helper_ret)
	{ 
		ERR_400();
		return; 
	}
	
	all_queues = podmq_queue_manager_get_queue_names(manager);

	builder = json_builder_new();
	json_builder_begin_object(builder);
	json_builder_set_member_name(builder, "queues");
	json_builder_begin_array(builder);

	for (
		i = 0;
		i < (gint)all_queues->len;
		i++
	){
		gchar *name;

		name = g_array_index(all_queues, gchar *, i);
		if (NULL != name)
		{ json_builder_add_string_value(builder, (const gchar *)name); }
	}

	json_builder_end_array(builder);
	json_builder_end_object(builder);

	root = json_builder_get_root(builder);
	generator = json_generator_new();
	json_generator_set_root(generator, root);

	if (TRUE == pretty)
	{
		json_generator_set_pretty(generator, TRUE);
		json_generator_set_indent(generator, 4);
	}

	ret = json_generator_to_data(generator, NULL);	


	soup_server_message_set_status(msg, SOUP_STATUS_OK, NULL);
	soup_server_message_set_response(
		msg,
		"application/json",
		SOUP_MEMORY_TAKE,
		(const gchar *)ret,
		strlen(ret)
	);
}




static 
void 
_podmq_apiv1_get_manager_summary(
	SoupServer			*server,
	SoupServerMessage		*msg,
	const gchar			*path,
	GHashTable			*query,
	gpointer			user_data
){
	PodMQServer *mq_server;
	PodMQQueue *queue;
	PodMQQueueManager *manager;
	gchar *ret;
	gchar *queue_name;
	gchar *manager_name;
	gboolean pretty;
	gboolean helper_ret;
	g_autoptr(JsonBuilder) builder;
	g_autoptr(JsonNode) root;
	g_autoptr(JsonGenerator) generator;
	g_autoptr(GArray) all_queues;
	gint i;


	mq_server = user_data;
	builder = NULL;
	root = NULL;
	generator = NULL;
	all_queues = NULL;

	helper_ret = _podmq_apiv1_simple_helper(
		server,
		msg,
		mq_server,
		query,
		&queue,
		&manager,
		&queue_name,
		&manager_name,
		&pretty
	);

	if (FALSE == helper_ret)
	{ 
		ERR_400();
		return; 
	}
	
	all_queues = podmq_queue_manager_get_queue_names(manager);

	builder = json_builder_new();
	json_builder_begin_object(builder);
	json_builder_set_member_name(builder, "queues");
	json_builder_begin_array(builder);

	for (
		i = 0;
		i < (gint)all_queues->len;
		i++
	){
		gchar *name;

		name = g_array_index(all_queues, gchar *, i);

		if (NULL != name)
		{ 	
			json_builder_begin_object(builder);
			json_builder_set_member_name(builder, "queue_name");
			json_builder_add_string_value(builder, (const gchar *)name); 
			json_builder_set_member_name(builder, "count");
			json_builder_add_int_value(
				builder, 
				podmq_queue_get_queue_item_count(
					podmq_queue_manager_get_queue_by_name(
						manager, 
						name
					)
				)
			);
			json_builder_end_object(builder);
		}
	}

	json_builder_end_array(builder);
	json_builder_end_object(builder);

	root = json_builder_get_root(builder);
	generator = json_generator_new();
	json_generator_set_root(generator, root);

	if (TRUE == pretty)
	{
		json_generator_set_pretty(generator, TRUE);
		json_generator_set_indent(generator, 4);
	}

	ret = json_generator_to_data(generator, NULL);	


	soup_server_message_set_status(msg, SOUP_STATUS_OK, NULL);
	soup_server_message_set_response(
		msg,
		"application/json",
		SOUP_MEMORY_TAKE,
		(const gchar *)ret,
		strlen(ret)
	);
}




static 
void 
_podmq_apiv1_peek(
	SoupServer			*server,
	SoupServerMessage		*msg,
	const gchar			*path,
	GHashTable			*query,
	gpointer			user_data
){
	PodMQServer *mq_server;
	PodMQQueue *queue;
	PodMQQueueManager *manager;
	gchar *ret;
	gchar *queue_name;
	gchar *manager_name;
	gboolean pretty;
	gboolean helper_ret;
	g_autoptr(JsonBuilder) builder;
	g_autoptr(JsonNode) root;
	g_autoptr(JsonGenerator) generator;
	g_autoptr(GQueue) queue_data;
	PodMQMessage *current;


	mq_server = user_data;
	builder = NULL;
	root = NULL;
	generator = NULL;
	queue_data = NULL;

	helper_ret = _podmq_apiv1_simple_helper(
		server,
		msg,
		mq_server,
		query,
		&queue,
		&manager,
		&queue_name,
		&manager_name,
		&pretty
	);

	if (FALSE == helper_ret)
	{ 
		ERR_400();
		return; 
	}
	
	queue_data = podmq_queue_peek(queue); 

	if (NULL == queue_data)
	{ 
		ERR_400();
		return;
	}

	builder = json_builder_new();
	json_builder_begin_array(builder);

	while (
		NULL != (current = g_queue_pop_head(queue_data))
	){
		JsonNode *n;
		
		n = json_node_copy(
			podmq_message_get_data(current)
		);

		json_builder_add_value(builder, n);
		g_object_unref(current);
	}

	json_builder_end_array(builder);

	root = json_builder_get_root(builder);
	generator = json_generator_new();
	json_generator_set_root(generator, root);

	if (TRUE == pretty)
	{
		json_generator_set_pretty(generator, TRUE);
		json_generator_set_indent(generator, 4);
	}

	ret = json_generator_to_data(generator, NULL);	


	soup_server_message_set_status(msg, SOUP_STATUS_OK, NULL);
	soup_server_message_set_response(
		msg,
		"application/json",
		SOUP_MEMORY_TAKE,
		(const gchar *)ret,
		strlen(ret)
	);
}




void 
podmq_apiv1_register_handlers(
	PodMQServer			*mq_server,
	SoupServer 			*soup_server
){
	g_return_if_fail(PODMQ_IS_SERVER(mq_server));
	g_return_if_fail(SOUP_IS_SERVER(soup_server));

	soup_server_add_handler(soup_server, "/api/v1/get", _podmq_apiv1_get, mq_server, NULL);
	soup_server_add_handler(soup_server, "/api/v1/put", _podmq_apiv1_put, mq_server, NULL);
	soup_server_add_handler(soup_server, "/api/v1/put_many", _podmq_apiv1_put_many, mq_server, NULL);
	soup_server_add_handler(soup_server, "/api/v1/create", _podmq_apiv1_create, mq_server, NULL);
	soup_server_add_handler(soup_server, "/api/v1/get_all_queue_names", _podmq_apiv1_get_all_queue_names, mq_server, NULL);
	soup_server_add_handler(soup_server, "/api/v1/get_manager_summary", _podmq_apiv1_get_manager_summary, mq_server, NULL);
	soup_server_add_handler(soup_server, "/api/v1/peek", _podmq_apiv1_peek, mq_server, NULL);
}



#endif