from flask import Flask, render_template, request 
from PodMQ import *


app = Flask(__name__)


@app.route("/")
def root():
	return render_template("index.html")


@app.route("/get")
def get():
	args = request.args
	queue = args.get("queue", default="default", type=str)
	manager = args.get("manager", default="default", type=str)
	api_token = args.get("api_token", default="", type=str)

	c = PodMQClient(manager=manager, queue=queue, api_token=api_token)
	m = c.get()

	return render_template("get.html", message=m)


@app.route("/peek")
def peek():
	args = request.args
	queue = args.get("queue", default="default", type=str)
	manager = args.get("manager", default="default", type=str)
	api_token = args.get("api_token", default="", type=str)

	c = PodMQClient(manager=manager, queue=queue, api_token=api_token)
	m = c.peek()
	
	return render_template("peek.html", messages=m)






def main():
	app.run(debug=True, host="0.0.0.0")



if __name__ == "__main__":
	main()
